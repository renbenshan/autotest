package com.webuy.autotest.dto;

import com.webuy.promotion.api.type.ConditionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author muze
 * @date 8/19/21 5:02 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreatePromotionDTO {
    /**
     * 促销名称
     */
    private String title;

    /**
     * 促销类型
     */
    private String promotionType;

    /**
     * 业务类型
     */
    private String businessCode;

    /**
     * 条件类型
     * @see ConditionType
     */
    private String conditionType;


    /**
     * 条件值
     */
    private String conditionValue;

    private String backConditionValue;

    /**
     * 动作值
     */
    private String actionValue;

    private String backActionValue;

    /**
     * @see com.webuy.promotion.api.type.ActionType
     */
    private String actionType;
    /**
     * 承担方
     */
    private Integer underTake;

    /**
     * 会场id
     */

    private Long exhibitionId;

    /**
     * 活动开始时间
     */
    private Date beginTime;
    /**
     * 活动结束时间
     */
    private Date endTime;


}
