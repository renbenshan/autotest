package com.webuy.autotest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author muze
 * @date 7/16/21 9:51 PM
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestDTO implements Serializable {


    private static final long serialVersionUID = 2466666151306397073L;

    private List<String> value;

}