package com.webuy.autotest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author muze
 * @date 8/27/21 11:57 AM
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CretaeSpuDTO {

    /**
     * 供应商ID
     * 鲸好麦：11757
     */
    private Long supplierId =11755L;

    /**
     * 子业务类型：301 鲸灵 ，327 鲸好麦
     */
    private Long subBizType;


    /**
     * 库存
     */
    private Integer inventory;


    /**
     * 原价
     */
    private Long originalPrice;

    /**
     * 供货价
     */
    private  Long supplierPrice;

    /**
     * 终端售价
     */
    private Long taobaoPrice;

    /**
     * 结算价格
     */
    private Long settlementPrice;

    /**
     * 三级类目ID
     * 女装：9662
     * 箱包：9903
     * 运动户外：711
     * 食品：10047
     */
    private Long wxhcCategoryId;

    /**
     * 品牌ID
     * 54180、54181、54182、54183
     *
     */
    private Long brandId;



}
