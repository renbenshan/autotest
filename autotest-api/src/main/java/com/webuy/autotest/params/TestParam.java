package com.webuy.autotest.params;

import lombok.Data;

import java.io.Serializable;

/**
 * @author muze
 * @date 7/16/21 9:34 PM
 */
@Data
public class TestParam implements Serializable {


    private static final long serialVersionUID = -210332078000303627L;

    private String param;

}