package com.webuy.autotest.apiImpl;

import com.aifocus.itemplatform.common.result.Result;
import com.aifocus.itemplatform.dto.pitem.CreatePitemParams;

import java.text.ParseException;
import java.util.List;

/**
 * @author muze
 * @date 8/17/21 3:39 PM
 */
public interface ExhibitionPitemApi {
    Result queryPitemList();

    com.aifocus.base.common.result.Result<List<Long>> addSpu(Long subBizType, Integer exhibitionParkType) throws ParseException;
}
