package com.webuy.autotest.apiImpl;

import com.aifocus.base.common.result.Result;
import com.aifocus.itemplatform.api.dto.SpuUploadDTO;
import com.webuy.autotest.dto.CretaeSpuDTO;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author muze
 * @date 8/26/21 10:37 PM
 * 商家的API：创建、编辑、同步spu等操作
 */

public interface SupplierCreateSpuApi {

    /**
     * 商家创建商品
     * @return
     */
     Result createSpu(CretaeSpuDTO cretaeSpuDTO);

}
