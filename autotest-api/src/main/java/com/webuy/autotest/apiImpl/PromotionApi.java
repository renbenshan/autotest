package com.webuy.autotest.apiImpl;

import com.webuy.autotest.dto.CreatePromotionDTO;
import com.webuy.promotion.api.type.Business;
import com.webuy.promotion.api.vo.Promotion;
import com.webuy.promotion.api.vo.Result;

import java.text.ParseException;

/**
 * @author muze
 * @date 8/19/21 4:12 PM
 */
public interface PromotionApi {

    /**
     * {@link Business}
     * {@link com.webuy.promotion.api.type.BusinessCode}
     *
     * @return
     */
    Result<Promotion> createPromotion(CreatePromotionDTO createPromotionDTO) throws ParseException;

}
