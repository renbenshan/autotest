package com.webuy.autotest.apiImpl;

import com.aifocus.base.common.result.Result;
import com.webuy.autotest.dto.TestDTO;
import com.webuy.autotest.params.TestParam;

/**
 * @author muze
 * @date 7/16/21 9:38 PM
 */
public interface TestApi {

    /**
     * 测试
     * @param param 参数
     * @return TestDTO
     */
    Result<TestDTO> test(TestParam param);


}
