package com.webuy.autotest.apiImpl;

import com.aifocus.base.common.result.Result;

import java.text.ParseException;

/**
 * @author muze
 * @date 8/12/21 6:04 PM
 */

public interface ExhibitionApi {

    Result creatExhibition(Integer tags,Integer exhibitionParkType) throws ParseException;

    /**
     * 蜂享家创建会场
     * @param exhibitionParkType
     * @return
     * @throws ParseException
     */
    Result createShExhibition(Integer exhibitionParkType) throws ParseException;
}
