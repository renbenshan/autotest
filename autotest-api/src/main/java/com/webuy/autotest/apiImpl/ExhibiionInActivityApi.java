package com.webuy.autotest.apiImpl;

import com.aifocus.base.common.result.Result;

import java.text.ParseException;

/**
 * @author muze
 * @date 8/17/21 11:26 AM
 */

public interface ExhibiionInActivityApi {


    Result addActivity() throws ParseException;

    /**
     * 获取活动列表
     * @param exhibitionParkId
     * @return
     * @throws ParseException
     */
    Result getSaleActivityList(Long exhibitionParkId) throws ParseException;


}
