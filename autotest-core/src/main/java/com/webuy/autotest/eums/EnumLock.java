package com.webuy.autotest.eums;

/**
 * @author muze
 * @date 7/19/21 1:10 AM
 */
public enum EnumLock {


    ;

    private String keyPrefix;
    /**
     * 等待锁 时间 毫秒
     */
    private Long waitTime;
    /**
     * 锁 超时时间 毫秒
     */
    private Long lockTime;
    EnumLock(String keyPrefix, Long waitTime, Long lockTime) {
        this.keyPrefix = keyPrefix;
        this.waitTime = waitTime;
        this.lockTime = lockTime;
    }

    public String getKeyPrefix() {
        return keyPrefix;
    }

    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    public Long getLockTime() {
        return lockTime;
    }

    public void setLockTime(Long lockTime) {
        this.lockTime = lockTime;
    }

    public Long getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(Long waitTime) {
        this.waitTime = waitTime;
    }
}
