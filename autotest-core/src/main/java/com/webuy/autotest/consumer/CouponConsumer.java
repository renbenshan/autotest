package com.webuy.autotest.consumer;
import com.webuy.couponcenter.api.CouponTemplateCrmApi;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author muze
 * @date 8/17/21 8:35 PM
 */
@Configuration
public class CouponConsumer {

    @Reference(interfaceName ="couponTemplateCrmApi",interfaceClass = CouponTemplateCrmApi.class,retries = 3, cluster = "failfast")
    private CouponTemplateCrmApi couponTemplateCrmApi;
    @Bean
    public CouponTemplateCrmApi couponTemplateCrmApi(){
        return couponTemplateCrmApi;
    }



}
