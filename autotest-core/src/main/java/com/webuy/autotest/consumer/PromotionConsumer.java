package com.webuy.autotest.consumer;

import com.webuy.promotion.api.PromotionBackendNewApi;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author muze
 * @date 8/19/21 4:16 PM
 */

@Configuration
public class PromotionConsumer {
    @Reference(interfaceName = "promotionBackendNewApi",interfaceClass = PromotionBackendNewApi.class,timeout = 3000,check = false)
    private PromotionBackendNewApi promotionBackendNewApi;

    @Bean
    public PromotionBackendNewApi promotionBackendNewApi(){
        return promotionBackendNewApi;
    }
}
