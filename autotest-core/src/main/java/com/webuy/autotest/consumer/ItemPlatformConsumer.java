package com.webuy.autotest.consumer;

import com.aifocus.itemplatform.api.*;
import com.aifocus.itemplatform.api.client.ItemClient;
import com.aifocus.itemplatform.api.item.ItemReadApiService;
import com.aifocus.itemplatform.api.item.ItemSearchApiService;
import com.aifocus.itemplatform.api.pitem.PitemStatusApiService;
import com.aifocus.wxhc.api.workflow.PitemApplyApiService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author muze
 * @date 7/17/21 11:27 AM
 */

@Configuration
public class ItemPlatformConsumer {
    /**
     * 商品
     */
    @Reference(interfaceName = "pitemApiService", interfaceClass = PitemApiService.class,  timeout = 6000, check = false)
    private PitemApiService pitemApiService;

    @Reference(interfaceName = "pitemStatusApiService", interfaceClass = PitemStatusApiService.class,  timeout = 5000, check = false)
    private PitemStatusApiService pitemStatusApiService;

    @Reference(interfaceName = "itemReadApiService", interfaceClass = ItemReadApiService.class,  timeout = 5000, check = false)
    private ItemReadApiService itemReadApiService;

    @Reference(interfaceName = "itemSearchApiService", interfaceClass = ItemSearchApiService.class,  timeout = 5000, check = false)
    private ItemSearchApiService itemSearchApiService;

    @Reference(interfaceName = "spuApiService",interfaceClass = SpuApiService.class,timeout = 3000,check = false)
    private SpuApiService spuApiService;

    @Reference(interfaceName = "wxhcPitemUploadApi",interfaceClass = WxhcPitemUploadApi.class,timeout = 3000,check = false)
    private WxhcPitemUploadApi wxhcPitemUploadApi;


    /**
     * 商品
     */
    @Reference(interfaceName = "itemCacheApi", interfaceClass = ItemCacheApi.class,  timeout = 3000, check = false)
    private ItemCacheApi itemCacheApi;

    /**
     * 商家系统APi,上传商品
     */
    @Reference(interfaceName = "wxhcSupUploadApi",interfaceClass = WxhcSupUploadApi.class, timeout = 3000,check = false)
    private WxhcSupUploadApi wxhcSupUploadApi;

    @Bean
    public PitemApiService pitemApiService(){
        return pitemApiService;
    }
    @Bean
    public ItemCacheApi itemCacheApi(){
        return itemCacheApi;
    }

    @Bean
    public PitemStatusApiService pitemStatusApiService(){
        return pitemStatusApiService;
    }
    @Bean
    public ItemReadApiService itemReadApiService(){
        return itemReadApiService;
    }
    @Bean
    public ItemSearchApiService itemSearchApiService(){
        return itemSearchApiService;
    }
    @Bean
    public SpuApiService spuApiService(){
        return spuApiService;
    }
    @Bean
    public WxhcPitemUploadApi wxhcPitemUploadApi(){
        return wxhcPitemUploadApi;
    }
    @Bean
    public WxhcSupUploadApi wxhcSupUploadApi(){
        return wxhcSupUploadApi;
    }
}
