package com.webuy.autotest.consumer;

import com.aifocus.itemplatform.api.PitemApiService;
import com.webuy.buy.api.PayOrderApi;
import com.webuy.buy.api.PayServiceApi;
import com.webuy.buy.api.order.SubBizOrderApi;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.xmlbeans.impl.xb.xmlconfig.Extensionconfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author muze
 * @date 8/4/21 4:10 PM
 */

@Configuration
public class PayOrderConsumer {
    @Reference(interfaceName = "payOrderApi", interfaceClass = PayOrderApi.class,  timeout = 3000, check = false,version = "1.0.0")
    private PayOrderApi payOrderApi;

    @Reference(interfaceName = "payServiceApi", interfaceClass = PayServiceApi.class,timeout = 3000, check = false)
    private PayServiceApi payServiceApi;

    @Reference(interfaceName = "subBizOrderApi",interfaceClass = SubBizOrderApi.class,timeout = 3000,check = false,version = "1.0.0")
    private SubBizOrderApi subBizOrderApi;

    @Bean
    public PayOrderApi payOrderApi(){
        return payOrderApi;
    }
    @Bean
    public PayServiceApi payServiceApi(){
        return payServiceApi;
    }
    @Bean
    public SubBizOrderApi subBizOrderApi(){
        return subBizOrderApi;
    }
}
