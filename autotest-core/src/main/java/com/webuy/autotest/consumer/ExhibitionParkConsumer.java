package com.webuy.autotest.consumer;

import com.aifocus.wxhc.api.ExhibitionParkApiService;
import com.aifocus.wxhc.api.HykExhibitionParkCategoryApiService;
import com.aifocus.wxhc.api.WxhcComponentConfigTemplateApi;
import com.webuy.exhibitionplatform.api.ExhibitionParkApi;
import com.webuy.exhibitionplatform.api.SaleActivityApi;
import com.webuy.exhibitionplatform.api.old.ExhibitionParkOldApi;
import com.webuy.exhibitionplatform.restructure.api.ExhibitionParkFacadeApi;
import com.webuy.exhibitionplatform.restructure.api.ExhibitionParkInnerFacadeApi;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author muze
 * @date 7/21/21 11:27 AM
 */

@Configuration
public class ExhibitionParkConsumer {

    @Reference(interfaceName = "exhibitionParkFacadeApi", interfaceClass = ExhibitionParkFacadeApi.class,  timeout = 3000, check = false,version = "1.0.0")
    private ExhibitionParkFacadeApi exhibitionParkFacadeApi;

    @Reference(interfaceName = "exhibitionParkApi", interfaceClass = ExhibitionParkApi.class, timeout = 3000 ,check = false, version = "1.0.0")
    private ExhibitionParkApi exhibitionParkApi;

    //创建会场：更新云仓、薇宝荟类目
    @Reference(interfaceName = "hykExhibitionParkCategoryApiService", interfaceClass = HykExhibitionParkCategoryApiService.class, timeout = 3000 ,check = false)
    private HykExhibitionParkCategoryApiService hykExhibitionParkCategoryApiService;

    //更新限制取消件数
    @Reference(interfaceName = "wxhcComponentConfigTemplateApi" ,interfaceClass = WxhcComponentConfigTemplateApi.class,timeout = 3000,check = false)
    private WxhcComponentConfigTemplateApi wxhcComponentConfigTemplateApi;

    //活动信息
    @Reference(interfaceName = "SaleActivityApi", interfaceClass = SaleActivityApi.class,timeout = 3000,check = false, version = "1.0.0")
    private SaleActivityApi saleActivityApi;

    @Reference(interfaceName = "exhibitionParkInnerFacadeApi",interfaceClass = ExhibitionParkInnerFacadeApi.class,timeout = 3000,check = false,version = "1.0.0")
    private ExhibitionParkInnerFacadeApi exhibitionParkInnerFacadeApi;


    @Bean
    public ExhibitionParkFacadeApi exhibitionParkFacadeApi(){
        return exhibitionParkFacadeApi;
    }

    @Bean
    public ExhibitionParkApi exhibitionParkApi(){
        return exhibitionParkApi;
    }

    @Bean
    public HykExhibitionParkCategoryApiService hykExhibitionParkCategoryApiService (){
        return hykExhibitionParkCategoryApiService;
    }

    @Bean
    public WxhcComponentConfigTemplateApi wxhcComponentConfigTemplateApi(){
        return wxhcComponentConfigTemplateApi;
    }
    @Bean
    public SaleActivityApi saleActivityApi(){
        return saleActivityApi;
    }
    @Bean
    public ExhibitionParkInnerFacadeApi exhibitionParkInnerFacadeApi(){
        return exhibitionParkInnerFacadeApi;
    }
}
