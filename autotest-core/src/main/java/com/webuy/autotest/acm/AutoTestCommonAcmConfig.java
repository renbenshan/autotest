package com.webuy.autotest.acm;

import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.Properties;

/**
 * @author muze
 * @date 7/16/21 10:10 PM
 */
@Slf4j
public class AutoTestCommonAcmConfig extends AbstractAcmConfig{


    private final static String DATA_ID = "whitewhale.common";

    private final static String GROUP_ID = "com.webuy.whitewhale";


    /**
     *******************  配置项开始 **********************
     */

    /**
     * dubbo 消费者日志打印比率
     */
    private static Long dubboCusumerLog = 1L;
    /**
     * http 日志打印比率
     */
    private static Long httpLog = 1L;







    @Override
    protected void loadProperties(Properties properties) {

        dubboCusumerLog = Long.valueOf(Optional.ofNullable(properties.getProperty("dubboCusumerLog")).orElse(dubboCusumerLog.toString()));

        httpLog = Long.valueOf(Optional.ofNullable(properties.getProperty("httpLog")).orElse(httpLog.toString()));

        log.info("------------------WhiteWhaleCommonAcmConfig init end------------------,properties={}",properties);
    }

    public static Long getDubboCusumerLog() {
        return dubboCusumerLog;
    }

    public static Long getHttpLog() {
        return httpLog;
    }

    @Override
    protected String getDataId() {
        return DATA_ID;
    }

    @Override
    protected String getGroupId() {
        return GROUP_ID;
    }
}
