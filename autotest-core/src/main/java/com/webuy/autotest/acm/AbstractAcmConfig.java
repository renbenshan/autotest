package com.webuy.autotest.acm;

import com.aifocus.base.acm.AcmClient;
import com.alibaba.edas.acm.ConfigService;
import com.alibaba.edas.acm.listener.ConfigChangeListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

/**
 * @author muze
 * @date 7/16/21 9:58 PM
 */
@Slf4j
public abstract class AbstractAcmConfig {

    // 配置存储
    private Properties properties = new Properties();

    @Resource
    private AcmClient acmClient;

    public void init() {

        // 初始化acm配置
        acmClient.init();

        String dataId = getDataId();
        String groupId = getGroupId();

        Assert.notNull(dataId, "dataId不能为空");
        Assert.notNull(groupId, "groupId不能为空");

        ConfigService.addListener(dataId, groupId, new ConfigChangeListener() {
            @Override
            public void receiveConfigInfo(String configInfo) {

                try {
                    log.info("receive config:{}", configInfo);

                    properties.load(new StringReader(configInfo));

                    loadProperties(properties);

                } catch (IOException e) {
                    log.error("获取配置更新异常",e);
                }
            }
        });

        // 这里要先主动获取下配置，避免在应用启动期间配置还没有推送到客户端导致客户端和服务端短暂的配置不一致
        try {
            String configInfo = ConfigService.getConfig(dataId, groupId, 6000);
            log.info("get config,dataid={},groupid={},content={}", dataId, groupId, configInfo);

            Properties autoGet = new Properties();
            autoGet.load(new StringReader(configInfo));
            loadProperties(autoGet);

        } catch (Exception e) {
            // 本地启动请注释
            throw new BeanCreationException("获取ACM配置异常", e);
        }

    }


    /**
     * 加载属性键值对
     *
     * @param properties
     */
    protected abstract void loadProperties(Properties properties);

    protected abstract String getDataId();

    protected abstract String getGroupId();



    public void setAcmClient(AcmClient acmClient) {
        this.acmClient = acmClient;
    }
}
