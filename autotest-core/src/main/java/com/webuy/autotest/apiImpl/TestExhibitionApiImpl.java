package com.webuy.autotest.apiImpl;

import com.aifocus.itemplatform.api.PitemApiService;
import com.aifocus.itemplatform.common.result.Result;
import com.aifocus.itemplatform.dto.WxhcPitemDTO;
import com.aifocus.itemplatform.query.WxhcPitemQuery;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author muze
 * @date 7/20/21 3:42 PM
 */

@Service
@Component
public class TestExhibitionApiImpl implements TestExhibitionApi {

    @Resource
    private PitemApiService pitemApiService;

    @Override
    public Result<List<WxhcPitemDTO>>queryDTO() {
        WxhcPitemQuery wxhcPitemQuery =new WxhcPitemQuery();
        wxhcPitemQuery.setExhibitionParkId(100315484L);

        return pitemApiService.queryDTO(wxhcPitemQuery);
    }
}
