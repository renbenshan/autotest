package com.webuy.autotest.apiImpl;

import com.aifocus.itemplatform.api.SpuApiService;
import com.aifocus.itemplatform.api.WxhcPitemUploadApi;
import com.aifocus.itemplatform.common.enums.EnumChannel;
import com.aifocus.itemplatform.common.enums.EnumPitemStatus;
import com.aifocus.itemplatform.common.result.Result;
import com.aifocus.itemplatform.dto.WxhcSpuDTO;
import com.aifocus.itemplatform.dto.pitem.CreatePitemParams;
import com.aifocus.itemplatform.query.WxhcSpuQuery;
import com.webuy.exhibitionplatform.request.ReleaseExhibitionParkRequest;
import com.webuy.exhibitionplatform.restructure.api.ExhibitionParkInnerFacadeApi;
import net.sf.json.JSONObject;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author muze
 * @date 8/17/21 3:44 PM
 */
@Service
public class ExhibitionPitemApiImpl implements ExhibitionPitemApi{
    @Autowired
    private SpuApiService spuApiService;
    @Autowired
    private WxhcPitemUploadApi wxhcPitemUploadApi;
    @Autowired
    private ExhibiionInActivityApi exhibiionInActivityApi;
    @Autowired
    private ExhibitionParkInnerFacadeApi exhibitionParkInnerFacadeApi;
    @Override
    public Result queryPitemList() {
        WxhcSpuQuery wxhcSpuQuery =new WxhcSpuQuery();
        /**
         * 商品类型：1 普通商品  2 平台商品  3 竞拍商品
         */
        wxhcSpuQuery.setItemType(1);
        wxhcSpuQuery.setSubBizType(301L);
        wxhcSpuQuery.setSupplierId(11755L);
        List<Integer> statusList =new ArrayList<>();

        /**
         * 1:上架，0：下架
         */
        statusList.add(1);
        wxhcSpuQuery.setStatusList(statusList);
        Result<List<WxhcSpuDTO>>  result=spuApiService.query(wxhcSpuQuery);
        return result;
    }

    @Override
    public com.aifocus.base.common.result.Result<List<Long>> addSpu(Long subBizType, Integer exhibitionParkType) throws ParseException {
        if(subBizType ==null&&exhibitionParkType==null){
            throw new RuntimeException("参数类型有误");
        }
        CreatePitemParams createPItemParams =new CreatePitemParams();
        Result<List<WxhcSpuDTO>> result =this.queryPitemList();
        List<WxhcSpuDTO> list =result.getEntry();
        com.aifocus.base.common.result.Result activityResult=exhibiionInActivityApi.addActivity();
        Object json =activityResult.getEntry();

        JSONObject data = JSONObject.fromObject(json);
        Object newEntry =data.getJSONObject("entry");
        JSONObject jsonObject = JSONObject.fromObject(newEntry);
        String activityId= jsonObject.get("saleActivityId").toString();

        String exhibitionId= jsonObject.get("exhibitionParkId").toString();
        List<Long> spuIdList = new ArrayList<>();
        for (WxhcSpuDTO wxhcSpuDTO:list){
            Long spuId = wxhcSpuDTO.getId();
            spuIdList.add(spuId);
        }
        createPItemParams.setSpuIdList(spuIdList);
        createPItemParams.setSubBizType(subBizType);
        createPItemParams.setTargetId(Long.valueOf(activityId));
        createPItemParams.setPitemStatus(EnumPitemStatus.ONLINE);
        if (subBizType == 301l){
            createPItemParams.setEnumChannel(EnumChannel.HYK);
        }else {
            createPItemParams.setEnumChannel(EnumChannel.BEE_HOME);
        }
        com.aifocus.base.common.result.Result<List<Long>> pitemResult=wxhcPitemUploadApi.createExhibitionPitem(subBizType,exhibitionParkType,createPItemParams);
        ReleaseExhibitionParkRequest releaseExhibitionParkRequest=new ReleaseExhibitionParkRequest();
        releaseExhibitionParkRequest.setExhibitionParkId(Long.valueOf(exhibitionId));
        com.aifocus.base.common.result.Result<Void> voidResult = exhibitionParkInnerFacadeApi.releaseExhibitionPark(releaseExhibitionParkRequest);

        return pitemResult;
    }
}