package com.webuy.autotest.apiImpl;

import com.aifocus.base.common.result.Result;
import com.alibaba.fastjson.JSONObject;
import com.webuy.exhibitionplatform.api.SaleActivityApi;
import com.webuy.exhibitionplatform.dto.CreateSaleActivityDTO;
import com.webuy.exhibitionplatform.dto.SaleActivityApiDTO;
import com.webuy.exhibitionplatform.enums.sort.EnumActivitySort;
import com.webuy.exhibitionplatform.query.SaleActivityQueryDTO;
import com.webuy.exhibitionplatform.request.CreateSaleActivityRequest;
import org.apache.commons.compress.utils.Lists;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.List;

/**
 * @author muze
 * @date 8/17/21 11:28 AM
 */

@Service
public class ExhibiionInActivityApiImpl implements ExhibiionInActivityApi{
    public static final Logger logger= LoggerFactory.getLogger(ExhibiionInActivityApiImpl.class);

    @Autowired
    private  SaleActivityApi saleActivityApi;
    @Autowired
    private ExhibitionApi exhibitionApi;

    @Override
    public Result addActivity() throws ParseException {
        CreateSaleActivityRequest createSaleActivityRequest =new CreateSaleActivityRequest();

        Integer tags = null;
        Integer exhibitionParkType = 1;
        Object exhibitionId =exhibitionApi.creatExhibition(tags,exhibitionParkType).getEntry();
        JSONObject jsonObject =new JSONObject();
        String exhibitionParkId =jsonObject.toJSONString(exhibitionId);
        System.out.println(exhibitionParkId);

        createSaleActivityRequest.setSaleActivityType(4);
        createSaleActivityRequest.setExhibitionParkId(Long.valueOf(exhibitionParkId));
        createSaleActivityRequest.setCategoryNameZh("沐泽自动化活动");
        createSaleActivityRequest.setCategoryNameEn("沐泽自动化活动");
        Result<CreateSaleActivityDTO> result=saleActivityApi.createSaleActivity(createSaleActivityRequest);
        CreateSaleActivityDTO createSaleActivityDTO =result.getEntry();

        logger.info("生成的会场ID："+createSaleActivityDTO.getExhibitionParkId()+"\n"+"生成的活动ID："+createSaleActivityDTO.getSaleActivityId());
        return Result.buildSuccess(result);
    }

    @Override
    public Result getSaleActivityList(Long exhibitionParkId) throws ParseException {
        if (null == exhibitionParkId) {
            return com.aifocus.base.common.result.Result.buildSuccess(Lists.newArrayList());
        }
        SaleActivityQueryDTO queryDTO = new SaleActivityQueryDTO();
        queryDTO.setExhibitionParkId(exhibitionParkId);
        queryDTO.setActivitySortCode(EnumActivitySort.PROCUREMENT_QUERY.getCode());
        Result<List<SaleActivityApiDTO>> result = saleActivityApi.querySaleActivityList(queryDTO);
        List<SaleActivityApiDTO> saleActivityDTOList = result.getEntry();
        for (int i = 0; i < saleActivityDTOList.size(); i++) {
            logger.info("获取的活动id："+ saleActivityDTOList.get(i).getSaleActivityId());
        }
        return Result.buildSuccess(result);
    }
}
