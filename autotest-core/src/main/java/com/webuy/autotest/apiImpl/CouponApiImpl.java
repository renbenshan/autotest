package com.webuy.autotest.apiImpl;

import com.aifocus.base.common.result.Result;
import com.aifocus.member.common.constant.BizTypeEnum;
import com.webuy.couponcenter.api.CouponTemplateCrmApi;
import com.webuy.couponcenter.common.constant.WxhcCouponConstant;
import com.webuy.couponcenter.common.enums.CouponProvideTypeEnum;
import com.webuy.couponcenter.common.enums.CouponTemplateStatusEnum;
import com.webuy.couponcenter.common.enums.EnumCostBearer;
import com.webuy.couponcenter.common.enums.EnumReceiveUserRange;
import com.webuy.couponcenter.domain.dto.ChangeCouponTemplateStatusDTO;
import com.webuy.couponcenter.domain.dto.CreateCouponParams;
import com.webuy.exhibitionplatform.api.ExhibitionParkApi;
import com.webuy.exhibitionplatform.dto.ExhibitionParkApiDTO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @author muze
 * @date 8/17/21 8:13 PM
 */

@Service
public class CouponApiImpl implements CouponApi{
    @Autowired
    private CouponTemplateCrmApi couponTemplateCrmApi;
    @Autowired
    private ExhibitionParkApi exhibitionParkApi;

    @Override
    public Result creatCouponWithexhition(Long exhibitionId) {
        CreateCouponParams createCouponParams =new CreateCouponParams();
        createCouponParams.setName("自动化创建优惠劵");
        createCouponParams.setSubBizType(301L);
        /**
         * @see BizTypeEnum
         */
        createCouponParams.setBizType(3L);
        /**
         * {@link com.webuy.couponcenter.common.enums.CouponFlagEnum}
         * 领取限制：21-全部用户
         */
        createCouponParams.setCouponFlag(21);
        //创建人
        createCouponParams.setCreatorId(2222243048695L);
        createCouponParams.setCreatorNick("自动化小公举");

        createCouponParams.setUsePlaceBody(exhibitionId);
        /**
         * {@link WxhcCouponConstant.USE_PLACE_TYPE_EXHIBITION_PARK}
         * 会场劵
         */
        createCouponParams.setUsePlaceType(1);
        /**
         * 是否跟随会场时间
         */
        createCouponParams.setUseExhibitionTime(true);
        createCouponParams.setAutomaticDischarge(0);
        /**
         * 是否同步云店
         */
        createCouponParams.setSynYundian(false);
        createCouponParams.setSynToOtherExhibition(true);
        List<Long> list =new ArrayList<>();
        list.add(301L);
        list.add(302L);
        list.add(308L);
        list.add(323L);
        createCouponParams.setUsePlaceBodys(list);
        /**
         * {@link EnumReceiveUserRange}
         * 使用范围：全部用户
         */
        createCouponParams.setReceiveUserRange(1);
        /**
         * {@link CouponProvideTypeEnum}
         * 发放类型：1-平台，2-商家
         */
        createCouponParams.setProvideType(1);
        /**
         * {@link EnumCostBearer}
         * 承担方：
         */
        createCouponParams.setCouponUndertaker(2);
        /**
         * 优惠劵的规则：满10 减5
         */
        createCouponParams.setPreferentialAmount(500L);
        createCouponParams.setConstraintAmount(1000L);

        Result<ExhibitionParkApiDTO> result=exhibitionParkApi.getExhibitionParkById(exhibitionId);
        ExhibitionParkApiDTO exhibitionParkApiDTO =result.getEntry();
        createCouponParams.setGmtStart(exhibitionParkApiDTO.getGmtStart());
        createCouponParams.setGmtEnd(exhibitionParkApiDTO.getGmtEnd());
        createCouponParams.setReceiveTimeStart(exhibitionParkApiDTO.getGmtStart());
        createCouponParams.setReceiveTimeEnd(exhibitionParkApiDTO.getGmtEnd());

        com.webuy.couponcenter.common.result.Result templateResult =
                couponTemplateCrmApi.createCouponTemplate(createCouponParams);
        String templateResultEntry =templateResult.getEntry().toString();
        ChangeCouponTemplateStatusDTO couponTemplateStatusDTO =new ChangeCouponTemplateStatusDTO();
        couponTemplateStatusDTO.setCouponTemplateId(Long.valueOf(templateResultEntry));

        /**
         * @see CouponTemplateStatusEnum
         * 优惠劵的状态
         *  GROUNDING_COUPON_TEMPL:上架状态
         */
        couponTemplateStatusDTO.setStatus(CouponTemplateStatusEnum.GROUNDING_COUPON_TEMPL.getValue());
        couponTemplateStatusDTO.setSubBizType(301L);
        couponTemplateCrmApi.putonCouponTemplate(couponTemplateStatusDTO);
        return Result.buildSuccess(templateResult);
    }
}
