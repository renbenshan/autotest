package com.webuy.autotest.apiImpl;

import com.aifocus.base.common.result.Result;
import com.aifocus.member.common.constant.BizTypeEnum;
import com.aifocus.member.common.constant.SubBizTypeEnum;
import com.aifocus.procurement.common.enums.wxhc.EnumCategoryType;
import com.aifocus.procurement.common.enums.wxhc.EnumComponentType;
import com.aifocus.procurement.common.enums.wxhc.EnumExhibitionParkCreatorType;
import com.aifocus.procurement.common.enums.wxhc.EnumPageConfigTemplate;
import com.aifocus.wxhc.api.HykExhibitionParkCategoryApiService;
import com.aifocus.wxhc.api.WxhcComponentConfigTemplateApi;
import com.aifocus.wxhc.dto.WxhcComponentConfigTemplateDTO;
import com.aifocus.wxhc.dto.WxhcExhibitionParkCategoryDTO;
import com.webuy.exhibitionplatform.api.ExhibitionParkApi;
import com.webuy.exhibitionplatform.api.SaleActivityApi;
import com.webuy.exhibitionplatform.dto.ExhibitionParkApiDTO;
import com.webuy.exhibitionplatform.dto.old.ExhibitionConfigDTO;
import com.webuy.exhibitionplatform.dto.old.ExhibitionParkAddOldReq;
import com.webuy.exhibitionplatform.dto.old.WxhcSaleActivityAddOldReq;
import com.webuy.exhibitionplatform.enums.EnumExhibitionOwnerType;
import com.webuy.exhibitionplatform.enums.EnumExhibitionParkFlag;
import com.webuy.exhibitionplatform.enums.EnumSaleActivityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author muze
 * @date 8/12/21 6:07 PM
 */
@Component
public class ExhibitionApiImpl implements ExhibitionApi {
    private static final Logger logger = LoggerFactory.getLogger(ExhibitionApiImpl.class);
    private static final String EXHIBITION_SERVICE_INFO = "会场客服信息";

    @Autowired
    private ExhibitionParkApi exhibitionParkApi;
    @Autowired
    private SaleActivityApi saleActivityApi;
    @Autowired
    private HykExhibitionParkCategoryApiService hykExhibitionParkCategoryApiService;
    @Autowired
    private WxhcComponentConfigTemplateApi wxhcComponentConfigTemplateApi;
    @Override
    public Result creatExhibition(Integer tags,Integer exhibitionParkType) throws ParseException {


        ExhibitionParkAddOldReq exhibitionParkAddOldReq =new ExhibitionParkAddOldReq();
        exhibitionParkAddOldReq.setExhibitionParkName("沐泽自动化创建测试会场");
        //会场类型：1普通会场
        if(exhibitionParkType ==null){
            exhibitionParkAddOldReq.setExhibitionParkType(1);
        }else {
            exhibitionParkAddOldReq.setExhibitionParkType(exhibitionParkType);
        }
        /**
         * @see com.webuy.exhibitionplatform.enums.EnumTagType
         */
        if(tags!=null){
            List<Integer> list =new ArrayList<>();
            list.add(tags);
            exhibitionParkAddOldReq.setTags(list);
        }


        //排序
        exhibitionParkAddOldReq.setExhibitionParkSort(10);

        /**
         *{@link BizTypeEnum}
         */
        exhibitionParkAddOldReq.setBizType(3L);
        exhibitionParkAddOldReq.setCategoryId(708L);
        exhibitionParkAddOldReq.setMarketCategoryId(312017L);
        ExhibitionConfigDTO.ExhibitionParkConfigObj exhibitionParkConfigObj =new ExhibitionConfigDTO.ExhibitionParkConfigObj();
        exhibitionParkConfigObj.setBrandId(53649L);
        exhibitionParkConfigObj.setBdId(132L);
        exhibitionParkConfigObj.setBenchmarking(false);
        //会场头图
        exhibitionParkConfigObj.setBrandImageUrl("assets/img/2021/08/13/n_1628826354752_3540___size1135x545.jpeg");
        exhibitionParkConfigObj.setBrandLogo("assets/img/2020/12/04/n_1607052218493_3145___size111x101.png");
        exhibitionParkConfigObj.setBrandName("沐泽Brands1");
        exhibitionParkConfigObj.setCategoryImage("assets/img/2021/08/13/n_1628826406733_6879___size800x800.jpeg");
        //折扣图
        exhibitionParkConfigObj.setHomepageBanner("assets/img/2021/08/13/n_1628826399005_6769___size702x280.jpeg");
        //会场图标
        exhibitionParkConfigObj.setIconImageUrl("assets/img/2020/12/04/n_1607052218493_3145___size111x101.png");
        exhibitionParkConfigObj.setIntrodu("会场简介");
        /**
         * 会场标签枚举类，见枚举{@link EnumExhibitionParkFlag}
         */
        exhibitionParkAddOldReq.setExhibitionFlag(1);
        exhibitionParkConfigObj.setMainMark("测试主标签");
        List secondlist =new ArrayList<>();
        secondlist.add(1);
        secondlist.add(2);
        secondlist.add(3);
        exhibitionParkConfigObj.setSecondMark(secondlist);
        /**
         * 首页商品图
         */
        List<String> list =new ArrayList<>();
        list.add("assets/img/2021/08/13/n_1628826413429_7288___size800x800.jpeg");
        list.add("assets/img/2021/08/13/n_1628826413429_7288___size800x800.jpeg");
        list.add("assets/img/2021/08/13/n_1628826413429_7288___size800x800.jpeg");
        exhibitionParkConfigObj.setHomepageImageUrl(list);

        exhibitionParkConfigObj.setSafeSow(false);
        //是否同步好衣库
        exhibitionParkConfigObj.setSyncToSH(true);
        //是否预热
        exhibitionParkConfigObj.setWarmUp(false);
        exhibitionParkConfigObj.setPostage(0);
        exhibitionParkConfigObj.setHykSortNum(10);
        exhibitionParkConfigObj.setAdvanceDelivery(false);
        /**
         * 提前购标记
         * 1: 提前购 会场 0: 非提前购会场
         * @see com.webuy.exhibitionplatform.enums.EnumYesOrNo
         */
        exhibitionParkConfigObj.setForwardBuyingFlag(1);

        exhibitionParkAddOldReq.setExhibitionParkConfigObj(exhibitionParkConfigObj);

        ExhibitionConfigDTO exhibitionConfigDTO =new ExhibitionConfigDTO();

        ExhibitionConfigDTO.ExhibitionParkLimitObj exhibitionParkLimitObj =new ExhibitionConfigDTO.ExhibitionParkLimitObj();
        exhibitionParkLimitObj.setExhibitionParkQuota(null);
        exhibitionParkLimitObj.setMinimumSpendThreshold(0);
        exhibitionParkLimitObj.setOutRelease(1);
        exhibitionParkAddOldReq.setExhibitionParkLimitObj(exhibitionParkLimitObj);

        /**
         * 创建人类型 {@link EnumExhibitionParkCreatorType}
         */
        exhibitionParkAddOldReq.setCreatorId(2222243048695L);
        exhibitionParkAddOldReq.setCreatorType(2);

        /**
         * 平台类型，见枚举{@link EnumExhibitionOwnerType}
         */
        exhibitionParkAddOldReq.setOwnerId(0L);
        exhibitionParkAddOldReq.setOwnerType(-1);

        exhibitionParkAddOldReq.setForever(false);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //初始化当前时间= 开始时间
        Date GmtStart =simpleDateFormat.parse(simpleDateFormat.format(new Date()));
        exhibitionParkAddOldReq.setGmtOnline(GmtStart);
        exhibitionParkAddOldReq.setGmtStart(GmtStart);

        String endTime=simpleDateFormat.format(new Date(GmtStart.getTime() + 24*60*60*1000));
        Date GmtEnd=simpleDateFormat.parse(endTime);

        exhibitionParkAddOldReq.setGmtEnd(GmtEnd);


        /**
         * 子业务类型{@link SubBizTypeEnum}
         */
        exhibitionParkAddOldReq.setSubBizType(301L);


        Result<Long> result =exhibitionParkApi.add(exhibitionParkAddOldReq);

        if(result.getEntry() !=null){
            //添加云仓的类目信息
            List<WxhcExhibitionParkCategoryDTO> wxhcExhibitionParkCategoryDTOList =new ArrayList<>();
            WxhcExhibitionParkCategoryDTO wxhcExhibitionParkCategoryDTO =new WxhcExhibitionParkCategoryDTO();
            wxhcExhibitionParkCategoryDTO.setCategoryName("女装大牌");
            /**
             *前台类目类型{@link EnumCategoryType}
             */
            wxhcExhibitionParkCategoryDTO.setCategoryType(308);
            wxhcExhibitionParkCategoryDTO.setExhibitionParkId(result.getEntry());
            wxhcExhibitionParkCategoryDTO.setWxhcConfigId(343L);
            wxhcExhibitionParkCategoryDTOList.add(wxhcExhibitionParkCategoryDTO);
            hykExhibitionParkCategoryApiService.insertExhibitionParkCategoryList(wxhcExhibitionParkCategoryDTOList);


            //添加薇宝荟的类目信息
            List<WxhcExhibitionParkCategoryDTO> wxhcExhibitionParkCategoryDTOList2 =new ArrayList<>();
            WxhcExhibitionParkCategoryDTO wxhcExhibitionParkCategoryDTO2 =new WxhcExhibitionParkCategoryDTO();
            wxhcExhibitionParkCategoryDTO2.setCategoryName("女装大牌");
            wxhcExhibitionParkCategoryDTO2.setCategoryType(328);
            wxhcExhibitionParkCategoryDTO2.setExhibitionParkId(result.getEntry());
            wxhcExhibitionParkCategoryDTO2.setWxhcConfigId(405L);
            wxhcExhibitionParkCategoryDTOList2.add(wxhcExhibitionParkCategoryDTO2);
            hykExhibitionParkCategoryApiService.insertExhibitionParkCategoryList(wxhcExhibitionParkCategoryDTOList2);


                //添加限制取消件数
            WxhcComponentConfigTemplateDTO wxhcComponentConfigTemplateDTO =new WxhcComponentConfigTemplateDTO();
            wxhcComponentConfigTemplateDTO.setExhibitionParkId(result.getEntry());
            wxhcComponentConfigTemplateDTO.setComponentType(EnumComponentType.TEXT.getCode());
            wxhcComponentConfigTemplateDTO.setComponentName(exhibitionParkAddOldReq.getExhibitionParkName()+EXHIBITION_SERVICE_INFO);
            wxhcComponentConfigTemplateDTO.setLinkUrl(result.getEntry().toString());
            wxhcComponentConfigTemplateDTO.setPageConfigTemplateId(EnumPageConfigTemplate.HAOYIKU_EXHIBITION_SERVICE.getId());
            wxhcComponentConfigTemplateDTO.setExhibitionLockCancelNum(1000);
            wxhcComponentConfigTemplateDTO.setCompomentUrl("");
            wxhcComponentConfigTemplateDTO.setServiceInfo("");


            System.out.println(wxhcComponentConfigTemplateDTO.toString());
        com.aifocus.procurement.common.result.Result result1 =wxhcComponentConfigTemplateApi.createComponentConfigTemplate(wxhcComponentConfigTemplateDTO);
        }

        logger.info("生成的会场ID："+result.getEntry());
        return result;
    }

    @Override
    public Result createShExhibition(Integer exhibitionParkType) throws ParseException {
        ExhibitionParkAddOldReq exhibitionParkAddOldReq =new ExhibitionParkAddOldReq();

        exhibitionParkAddOldReq.setExhibitionParkName("冷萃自动化创建测试会场");

        // 会场类型，1线上／2线下/3测试/4私密会场/5预告/6积分商城会场/7营销会场/8爆款推荐会场/9新人会场
        if (exhibitionParkType == null) {
            exhibitionParkAddOldReq.setExhibitionParkType(1);
        }else {
            exhibitionParkAddOldReq.setExhibitionParkType(exhibitionParkType);
        }

        // 后续根据会场类型，进行判断赋值
        // 设置开始时间=当前时间，结束时间=当前时间+1天
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date GmtStart =simpleDateFormat.parse(simpleDateFormat.format(new Date()));
        exhibitionParkAddOldReq.setGmtOnline(GmtStart);
        exhibitionParkAddOldReq.setGmtStart(GmtStart);

        String endTime=simpleDateFormat.format(new Date(GmtStart.getTime() + 24*60*60*1000));
        Date GmtEnd=simpleDateFormat.parse(endTime);
        exhibitionParkAddOldReq.setGmtEnd(GmtEnd);

        /**
         *业务类型{@link BizTypeEnum}
         */
        exhibitionParkAddOldReq.setBizType(3L);

        /**
         * 子业务类型{@link SubBizTypeEnum}
         */
        exhibitionParkAddOldReq.setSubBizType(323L);

        // 是否是永久会场
        exhibitionParkAddOldReq.setForever(false);

        // 营销类目
        exhibitionParkAddOldReq.setMarketCategoryId(312000L);

        // 排序值
        exhibitionParkAddOldReq.setExhibitionParkSort(10);

        /**
         * 会场标签枚举类，见枚举{@link EnumExhibitionParkFlag}
         */
        exhibitionParkAddOldReq.setExhibitionFlag(1);

        /**
         * 创建人类型 {@link EnumExhibitionParkCreatorType}
         */
        exhibitionParkAddOldReq.setCreatorId(2222243048695L);
        exhibitionParkAddOldReq.setCreatorType(2);

        /**
         * 拥有者，见枚举{@link EnumExhibitionOwnerType}
         */
        exhibitionParkAddOldReq.setOwnerId(0L);
        exhibitionParkAddOldReq.setOwnerType(-1);

        ExhibitionConfigDTO.ExhibitionParkConfigObj exhibitionParkConfigObj = new ExhibitionConfigDTO.ExhibitionParkConfigObj();
        exhibitionParkConfigObj.setBdId(132L);
        /**
         * 品牌标签
         * See Also:
         * com.webuy.exhibitionplatform.enums.EnumExhibitionBrandTag, com.webuy.exhibitionplatform.enums.EnumHYKExhibitionBrandTag
         */
        exhibitionParkConfigObj.setBrandTag(1);
        exhibitionParkConfigObj.setFlagForExhibitionPark(0);
        /**
         * 提前购标记
         * 1: 提前购 会场 0: 非提前购会场
         * @see com.webuy.exhibitionplatform.enums.EnumYesOrNo
         */
        exhibitionParkConfigObj.setForwardBuyingFlag(1);
        //折扣图
        exhibitionParkConfigObj.setHomepageBanner("assets/img/2021/08/13/n_1628826399005_6769___size702x280.jpeg");
        exhibitionParkConfigObj.setIntrodu("测试会场简介");
        exhibitionParkConfigObj.setOneSentenceTitle("测试一句话标签");
        //定时发布时间 后续作为参数判断
        exhibitionParkConfigObj.setTimingReleaseTime(null);

        ExhibitionParkApiDTO.ExhibitionParkConfigObj.BrandDiscountConfig brandDiscountConfig = new ExhibitionParkApiDTO.ExhibitionParkConfigObj.BrandDiscountConfig();
        List<ExhibitionParkApiDTO.ExhibitionParkConfigObj.BrandDiscountConfig.BrandLogo> brandLogos = new ArrayList<>();
        ExhibitionParkApiDTO.ExhibitionParkConfigObj.BrandDiscountConfig.BrandLogo brandLogo = new ExhibitionParkApiDTO.ExhibitionParkConfigObj.BrandDiscountConfig.BrandLogo();
        brandLogo.setBrandId(53649L);
        brandLogo.setBrandLogo("assets/img/2020/12/04/n_1607052334536_7438___size1200x1200.jpeg");
        brandLogo.setBrandName("沐泽Brands1");
        brandLogos.add(brandLogo);
        brandDiscountConfig.setBrandLogos(brandLogos);
        exhibitionParkConfigObj.setBrandDiscountConfig(brandDiscountConfig);
        exhibitionParkAddOldReq.setExhibitionParkConfigObj(exhibitionParkConfigObj);

        ExhibitionConfigDTO.ExhibitionParkLimitObj exhibitionParkLimitObj =new ExhibitionConfigDTO.ExhibitionParkLimitObj();
        exhibitionParkLimitObj.setExhibitionParkQuota(null);
        exhibitionParkLimitObj.setMinimumSpendThreshold(0);
        exhibitionParkLimitObj.setOutRelease(0);
        exhibitionParkAddOldReq.setExhibitionParkLimitObj(exhibitionParkLimitObj);

        Result<Long> addExhibitionResult =exhibitionParkApi.add(exhibitionParkAddOldReq);
        logger.info("生成的会场ID："+addExhibitionResult.getEntry());

        //新建活动模板
        WxhcSaleActivityAddOldReq wxhcSaleActivityAddOldReq = new WxhcSaleActivityAddOldReq();
        wxhcSaleActivityAddOldReq.setBizType(exhibitionParkAddOldReq.getBizType());
        wxhcSaleActivityAddOldReq.setShopId(exhibitionParkAddOldReq.getShopId());
        wxhcSaleActivityAddOldReq.setCategoryNameZh(exhibitionParkAddOldReq.getExhibitionParkName());
        wxhcSaleActivityAddOldReq.setExhibitionParkId(addExhibitionResult.getEntry());
        wxhcSaleActivityAddOldReq.setSaleActivityType(EnumSaleActivityType.UNIVERSAL.getCode());
        Long saleActivityId;
        try {
            Result<Long> addActivityResult = saleActivityApi.add(wxhcSaleActivityAddOldReq);
            if (!addActivityResult.isStatus()) {
                exhibitionParkApi.delete(addExhibitionResult.getEntry());
                return Result.buildFail(addActivityResult.getMessage());
            }
            saleActivityId = addActivityResult.getEntry();
        } catch (Exception e) {
            exhibitionParkApi.delete(addExhibitionResult.getEntry());
            return Result.buildFail("会场创建失败！");
        }
        // 结果为null  后续修改
        if(addExhibitionResult.getEntry() !=null){
            //添加限制取消件数
            WxhcComponentConfigTemplateDTO wxhcComponentConfigTemplateDTO =new WxhcComponentConfigTemplateDTO();
            wxhcComponentConfigTemplateDTO.setExhibitionParkId(addExhibitionResult.getEntry());
            wxhcComponentConfigTemplateDTO.setComponentType(EnumComponentType.TEXT.getCode());
            wxhcComponentConfigTemplateDTO.setComponentName(exhibitionParkAddOldReq.getExhibitionParkName()+EXHIBITION_SERVICE_INFO);
            wxhcComponentConfigTemplateDTO.setLinkUrl(addExhibitionResult.getEntry().toString());
            wxhcComponentConfigTemplateDTO.setPageConfigTemplateId(EnumPageConfigTemplate.HAOYIKU_EXHIBITION_SERVICE.getId());
            wxhcComponentConfigTemplateDTO.setExhibitionLockCancelNum(100);
            wxhcComponentConfigTemplateDTO.setCompomentUrl("");
            wxhcComponentConfigTemplateDTO.setServiceInfo("");

            com.aifocus.procurement.common.result.Result result1 =wxhcComponentConfigTemplateApi.createComponentConfigTemplate(wxhcComponentConfigTemplateDTO);
            logger.info("生成的Component："+result1.getEntry());
        }

        return addExhibitionResult;

    }
}
