package com.webuy.autotest.apiImpl;

import com.aifocus.base.common.result.Result;
import com.aifocus.itemplatform.api.WxhcSupUploadApi;
import com.aifocus.itemplatform.api.dto.SkuUploadDTO;
import com.aifocus.itemplatform.api.dto.SpuUploadDTO;
import com.aifocus.itemplatform.common.enums.SpuComeFormEnums;
import com.aifocus.itemplatform.dto.WxhcSkuDTO;
import com.aifocus.itemplatform.dto.WxhcSpuDTO;
import com.webuy.autotest.dto.CretaeSpuDTO;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author muze
 * @date 8/26/21 10:41 PM
 */

@Service
public class SupplierCreateSpuApiImpl implements SupplierCreateSpuApi{



    @Autowired
    private WxhcSupUploadApi wxhcSupUploadApi;

    @Override
    public Result createSpu(CretaeSpuDTO cretaeSpuDTO) {

        List<SpuUploadDTO> list =new ArrayList<>();

        SpuUploadDTO spuUploadDTO =new SpuUploadDTO();

        spuUploadDTO.setWxhcSupplierId(cretaeSpuDTO.getSupplierId());
        Random random =new Random(10);
        Integer code =random.nextInt();
        String stringCode= RandomStringUtils.randomAlphanumeric(10);
        String spuCode = stringCode +code;
        spuUploadDTO.setSupplierSpuCode(spuCode);
        String name = "自动化创建商品" + "_"+stringCode ;
        spuUploadDTO.setName(name);
        spuUploadDTO.setBrandId(cretaeSpuDTO.getBrandId());

        spuUploadDTO.setBatchNum(1231231);
        WxhcSpuDTO.SpuAttributes spuAttributes =new WxhcSpuDTO.SpuAttributes();
        spuAttributes.setAttribute1("颜色");
        spuAttributes.setAttribute2("尺码");
        spuAttributes.setHeadPictureUrl("assets/img/2021/08/26/n_1629987151006_8093___size1200x1200.jpeg");
        spuAttributes.setSizePictureUrl(null);
        spuUploadDTO.setAttributes(spuAttributes);


        SkuUploadDTO skuUploadDTO =new SkuUploadDTO();
        WxhcSkuDTO.SkuAttributes skuAttributes = new WxhcSkuDTO.SkuAttributes();

        skuUploadDTO.setBarcode("2332453242");

        skuUploadDTO.setInventory(cretaeSpuDTO.getInventory());
        skuUploadDTO.setOriginalPrice(cretaeSpuDTO.getOriginalPrice());
        skuUploadDTO.setSupplierPrice(cretaeSpuDTO.getSupplierPrice());
        skuUploadDTO.setTaobaoPrice(cretaeSpuDTO.getTaobaoPrice());
        skuUploadDTO.setSettlementPrice(cretaeSpuDTO.getSettlementPrice());
        String[] headPictures= {
                "assets/img/2021/08/27/n_1630047424355_7515___size800x800.jpeg"
        };
        skuUploadDTO.setHeadPictures(headPictures);
        skuAttributes.setAttribute1("红色");
        skuAttributes.setAttribute2("XL");
        skuUploadDTO.setAttributes(skuAttributes);
        List<SkuUploadDTO> skuList= new ArrayList<>();

        skuList.add(skuUploadDTO);
        spuUploadDTO.setWxhcSkuDTOS(skuList);
        String[] headPicturesArray = {
                "assets/img/2021/08/26/n_1629987155219_8068___size1200x1200.jpeg",
                "assets/img/2021/08/26/n_1629987155219_8068___size1200x1200.jpeg",
                "assets/img/2021/08/26/n_1629987155219_8068___size1200x1200.jpeg",
                "assets/img/2021/08/26/n_1629987155219_8068___size1200x1200.jpeg",
                "assets/img/2021/08/26/n_1629987155219_8068___size1200x1200.jpeg"
        };
        spuUploadDTO.setHeadPictures(headPicturesArray);
        spuUploadDTO.setSubBizType(cretaeSpuDTO.getSubBizType());
        spuUploadDTO.setBizType(3L);
        spuUploadDTO.setWxhcCategoryId(cretaeSpuDTO.getWxhcCategoryId());

        spuUploadDTO.setSpuComeFrom(SpuComeFormEnums.MERCHANT.getCode());
        spuUploadDTO.setStatus(1);

        list.add(spuUploadDTO);
        Result result =wxhcSupUploadApi.saveSpu(list,null);

        return result;
    }
}
