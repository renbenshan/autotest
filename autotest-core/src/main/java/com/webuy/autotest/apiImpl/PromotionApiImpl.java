package com.webuy.autotest.apiImpl;

import com.webuy.autotest.dto.CreatePromotionDTO;
import com.webuy.exhibitionplatform.api.ExhibitionParkApi;
import com.webuy.exhibitionplatform.dto.ExhibitionParkApiDTO;
import com.webuy.promotion.api.PromotionBackendNewApi;
import com.webuy.promotion.api.req.CreatePromotionReq;
import com.webuy.promotion.api.type.ApplyScopeType;
import com.webuy.promotion.api.type.Business;
import com.webuy.promotion.api.vo.Promotion;
import com.webuy.promotion.api.vo.Result;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author muze
 * @date 8/19/21 4:36 PM
 */
@Service
public class PromotionApiImpl implements PromotionApi{
    @Autowired
    private PromotionBackendNewApi promotionBackendNewApi;
    @Autowired
    private ExhibitionParkApi exhibitionParkApi;

    @Override
    public Result<Promotion> createPromotion(CreatePromotionDTO createPromotionDTO) throws ParseException {

        CreatePromotionReq createPromotionReq =new CreatePromotionReq();
        createPromotionReq.setTitle(createPromotionDTO.getTitle());
        /**
         * {@link Business}
         */
        createPromotionReq.setBusiness(createPromotionDTO.getPromotionType());
        /**
         * @see com.webuy.promotion.api.type.BusinessCode
         */
        createPromotionReq.setBusinessCode(createPromotionDTO.getBusinessCode());
        createPromotionReq.setActionType(createPromotionDTO.getActionType());
        createPromotionReq.setActionValue(createPromotionDTO.getActionValue());
        createPromotionReq.setBackActionValue(createPromotionDTO.getBackActionValue());

        createPromotionReq.setConditionType(createPromotionDTO.getConditionType());
        createPromotionReq.setConditionValue(createPromotionDTO.getConditionValue());

        /**
         * 费用承担方 0平台承担，1商家承担
         */
        createPromotionReq.setUndertake(createPromotionDTO.getUnderTake());

        createPromotionReq.setExhibitionParkId(createPromotionDTO.getExhibitionId());
        createPromotionReq.setBeginTime(createPromotionDTO.getBeginTime());
        createPromotionReq.setEndTime(createPromotionDTO.getEndTime());

        /**
         * @see ApplyScopeType
         * 3：会场
         */
        createPromotionReq.setApplyScopeType(3);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date currentTime =simpleDateFormat.parse(simpleDateFormat.format(new Date()));
        String startTime=simpleDateFormat.format(new Date(currentTime.getTime() + 24*60*60*1));
        Date  brginTime=simpleDateFormat.parse(startTime);

        createPromotionReq.setBeginTime(brginTime);

        ExhibitionParkApiDTO exhibitionParkApiDTO =exhibitionParkApi.getExhibitionParkById(createPromotionDTO.getExhibitionId()).getEntry();

        createPromotionReq.setEndTime(exhibitionParkApiDTO.getGmtEnd());


        List<Long> list =new ArrayList<>();
        list.add(exhibitionParkApiDTO.getExhibitionParkId());
        createPromotionReq.setApplyScopeList(list);

        com.webuy.promotion.api.vo.Result<Promotion> result = promotionBackendNewApi.generalCreatePromotion(createPromotionReq);
        Promotion promotionResult =result.getEntry();
        Promotion promotion =new Promotion();
        promotion.setPromotionId(promotionResult.getPromotionId());
        promotion.setStatus(6);
        Result<Boolean> publishResult= promotionBackendNewApi.publishPromotion(promotion);


        return result;
    }
}
