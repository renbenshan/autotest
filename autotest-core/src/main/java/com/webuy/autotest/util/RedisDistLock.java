package com.webuy.autotest.util;

import com.webuy.autotest.eums.EnumLock;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.util.Assert;

import java.util.concurrent.TimeUnit;

/**
 * @author muze
 * @date 7/19/21 1:09 AM
 */
public class RedisDistLock {


    private String redisHost;
    private String redisPort;
    private String redisPasswd;

    private RedissonClient redissonClient;

    public void init() {

        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + redisHost + ":" + redisPort).setPassword(redisPasswd);
        redissonClient = Redisson.create(config);

    }


    /**
     * @param enumLock
     * @param args   keys
     * @return
     */
    public boolean tryLock(EnumLock enumLock, String... args) {
        String key = enumLock.getKeyPrefix() + String.join("_", args);
        RLock lock = redissonClient.getLock(key);
        try {
            return lock.tryLock(enumLock.getWaitTime(), enumLock.getLockTime(), TimeUnit.MILLISECONDS);

        } catch (InterruptedException e) {
            throw new RuntimeException("try Get Distributed Lock fail");
        }
    }


    /**
     * @param key
     * @param waitTime   等待时间单位毫秒
     * @param expireTime 锁自动失效时间 单位毫秒
     * @return
     */
    public boolean tryLock(String key, long waitTime, long expireTime) {
        RLock lock = redissonClient.getLock(key);
        try {
            return lock.tryLock(waitTime, expireTime, TimeUnit.MILLISECONDS);

        } catch (InterruptedException e) {
            throw new RuntimeException("try Get Distributed Lock fail");
        }

    }

    public void unlock(EnumLock enumLock, String... args) {
        String key = enumLock.getKeyPrefix() + String.join("_", args);
        RLock lock = redissonClient.getLock(key);
        lock.unlock();
    }

    public void unlock(String key) {
        RLock lock = redissonClient.getLock(key);
        lock.unlock();
    }

    public String buildBizKey(Object... params) {
        Assert.isTrue(params != null && params.length > 0, "参数不能为空");

        StringBuilder ans = new StringBuilder();
        for (Object param : params) {
            if (ans.length() != 0) {
                ans.append("#");
            }
            ans.append(String.valueOf(param));
        }

        return ans.toString();
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public void setRedisPort(String redisPort) {
        this.redisPort = redisPort;
    }

    public void setRedisPasswd(String redisPasswd) {
        this.redisPasswd = redisPasswd;
    }

    public RedissonClient getRedissonClient() {
        return redissonClient;
    }
}