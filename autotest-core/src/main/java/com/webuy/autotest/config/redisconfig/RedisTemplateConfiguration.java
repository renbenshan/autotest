package com.webuy.autotest.config.redisconfig;

import com.webuy.autotest.config.JacksonSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author muze
 * @date 7/19/21 1:11 AM
 */
@Configuration
@Component
public class RedisTemplateConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(RedisTemplateConfiguration.class);

    @Value("${env}")
    private String env;

    @Value("${autotest.redis.host}")
    private String redisHost;

    @Value("${autotest.redis.port}")
    private Integer redisPort;



    @Resource
    private RedisConnectionFactory redisConnectionFactory;


    @Bean
    public RedisTemplate redisTemplate() {
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        RedisSerializer jacksonSerializer = new JacksonSerializer();
        RedisSerializer stringRedisSerializer = new StringRedisSerializer();
        // key 的序列化采用 StringRedisSerializer
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        // value 值的序列化采用 GenericJackson2JsonRedisSerializer
        redisTemplate.setValueSerializer(jacksonSerializer);
        redisTemplate.setHashValueSerializer(jacksonSerializer);

        // 必须执行这个函数,初始化RedisTemplate
        redisTemplate.afterPropertiesSet();
        logger.info("RedisTemplate init success  env is {} , host is {} ,port is {} ",
                env, redisHost, redisPort);
        return redisTemplate;
    }




}