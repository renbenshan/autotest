package com.webuy.autotest.config.redisconfig;

import com.webuy.autotest.util.RedisDistLock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author muze
 * @date 7/19/21 1:07 AM
 */
@Configuration
public class RedisDistLockConfiguration {



    @Value("${autotest.redis.host}")
    private String redisHost;

    @Value("${autotest.redis.port}")
    private Integer redisPort;

    @Value("${autotest.redis.password}")
    private String redisPassword;


    @Bean("autotestRedisDistLock")
    @Primary
    public RedisDistLock getRedisDistLock(){
        RedisDistLock redisDistLock = new RedisDistLock();
        redisDistLock.setRedisHost(redisHost);
        redisDistLock.setRedisPort(String.valueOf(redisPort));
        redisDistLock.setRedisPasswd(redisPassword);
        redisDistLock.init();
        return redisDistLock;
    }

}
