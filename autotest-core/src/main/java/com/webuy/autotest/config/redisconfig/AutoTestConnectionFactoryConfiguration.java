package com.webuy.autotest.config.redisconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

/**
 * @author muze
 * @date 7/19/21 1:12 AM
 */

@Configuration
public class AutoTestConnectionFactoryConfiguration {

    @Value("${autotest.redis.host}")
    private String redisHost;

    @Value("${autotest.redis.port}")
    private Integer redisPort;

    @Value("${autotest.redis.password}")
    private String redisPassword;

    @Value("${autotest.redis.index}")
    private Integer databaseIndex;

    @Bean
    @Primary
    RedisConnectionFactory getConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setPort(redisPort);
        redisStandaloneConfiguration.setHostName(redisHost);
        redisStandaloneConfiguration.setDatabase(databaseIndex);
        redisStandaloneConfiguration.setPassword(RedisPassword.of(redisPassword));
        JedisConnectionFactory redisStandaloneConfigurationFactory = new JedisConnectionFactory(redisStandaloneConfiguration);

        return redisStandaloneConfigurationFactory;
    }
}
