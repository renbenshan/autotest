package com.webuy.autotest.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author muze
 * @date 7/16/21 10:21 PM
 */

@Configuration
@Slf4j
public class DubboConfiguration {

    @Value("${env}")
    private String env;
    @Value("${dubbo.group}")
    private String dubboGroup;

    @Value("${dubbo_zk_url}")
    private String defaultRegisterHost;
    @Value("${dubbo.server.port}")
    private Integer defaultRegisterPort;
    @Value("${dubbo.register.switch}")
    private boolean defaultZkRegister;
    @Value("${dubbo.pro.zk.list}")
    private String dubboProZklist;
    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${dubbo.local.port}")
    private Integer localPort;
    @Value("${dubbo.name}")
    private String dubboName;
    @Value("${dubbo.serialization}")
    private String dubboSerialization;

    //注册中心类型
    private final String REGISTER_TYPE = "zookeeper:";

    @Bean
    public ApplicationConfig applicationConfig() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName(applicationName);
        return applicationConfig;
    }

    @Bean
    public ProtocolConfig protocolConfig(){
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setSerialization(dubboSerialization);
        protocolConfig.setPort(localPort);
        protocolConfig.setName(dubboName);

        return protocolConfig;
    }

    @Bean
    public ConsumerConfig consumerConfig() {
        ConsumerConfig consumerConfig = new ConsumerConfig();
        if (isUserGroup()) {
            consumerConfig.setGroup(dubboGroup);
        }
//        consumerConfig.setFilter("dubboConsumerFilter");
        return consumerConfig;
    }

    @Bean
    public ProviderConfig providerConfig() {
        ProviderConfig providerConfig = new ProviderConfig();
        if (isUserGroup()) {
            providerConfig.setGroup(dubboGroup);
        }
//        providerConfig.setFilter("dubboProviderFilter");
        //dubbo服务的发布服务，可以向多个注册中心发布注册
        providerConfig.setRegistries(this.getPubRegistry());
        return providerConfig;
    }

    /**
     * 是否使用组别配置、默认本地和开发环境开启
     * 灰度和线上不开启
     *
     * @return 是否使用
     */
    private boolean isUserGroup() {

        List<String> useDubboGroupEnvList = Arrays.asList("daily", "local", "dev_sh");

        return useDubboGroupEnvList.contains(env);
    }

    /**
     * 获取订阅注册中心地址
     */
    public RegistryConfig getSubRegistry() {
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(REGISTER_TYPE + "//" + defaultRegisterHost + ":" + defaultRegisterPort);
        registryConfig.setRegister(defaultZkRegister);
        return registryConfig;
    }

    @Bean
    public RegistryConfig registryConfig() {
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(REGISTER_TYPE + "//" + defaultRegisterHost + ":" + defaultRegisterPort);
        registryConfig.setRegister(defaultZkRegister);
        return registryConfig;
    }

    /**
     * 获取发布的注册中心地址
     */
    public List<RegistryConfig> getPubRegistry() {
        List<RegistryConfig> registryConfigs = new ArrayList<RegistryConfig>();
        //添加订阅注册中心地址
        registryConfigs.add(getSubRegistry());
        if (!("gray".equals(env)) && (!"online".equals(env))) {
            //灰度 和线上环境，不做多注册中心注册服务
            if (!StringUtils.isEmpty(this.dubboProZklist)) {
                //注册中心配置格式按 地址:端口号:是否保留服务|地址:端口号:是否保留服务
                String[] registers = dubboProZklist.split("\\|");
                for (String register : registers) {
                    String[] registerMs = register.split(":");
                    RegistryConfig registryConfig = new RegistryConfig();
                    registryConfig.setAddress(REGISTER_TYPE + "//" + registerMs[0] + ":" + registerMs[1]);
                    registryConfig.setRegister(Boolean.parseBoolean(registerMs[2]));
                    registryConfigs.add(registryConfig);
                    log.error("pub other registory address :" + register);
                }
            } else {
                log.error("pub other registory address is empty");
            }
        }
        return registryConfigs;
    }

    /**
     * 元数据管理中心地址
     */
    @Bean
    public MetadataReportConfig metadataReportRegistry() {
        MetadataReportConfig metadataReportConfig = new MetadataReportConfig();
        metadataReportConfig.setAddress(REGISTER_TYPE + "//" + defaultRegisterHost + ":" + defaultRegisterPort);
        return metadataReportConfig;
    }
}