package com.webuy.autotest.config;

import com.aifocus.base.acm.AcmClient;
import com.webuy.autotest.acm.AutoTestCommonAcmConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author muze
 * @date 7/16/21 10:20 PM
 */
@Configuration
public class AcmConfiguration {


    @Value("${env}")
    private String env;

    @Value("${alibaba.acm.endpoint}")
    private String endpoint;

    @Value("${alibaba.acm.namespace}")
    private String namespace;

    @Value("${alibaba.acm.accessKey}")
    private String accessKey;

    @Value("${alibaba.acm.secretKey}")
    private String secretKey;


    /**
     * 初始化acmClient
     */
    @Bean(initMethod = "init")
    AcmClient acmClient() {
        AcmClient acmClient=new AcmClient();
        acmClient.setAccessKey(accessKey);
        acmClient.setNamespace(namespace);
        acmClient.setEndpoint(endpoint);
        acmClient.setSecretKey(secretKey);
        return acmClient;
    }


    @Bean
    public AutoTestCommonAcmConfig autotestCommonAcmConfig(AcmClient acmClient){
        AutoTestCommonAcmConfig autotestCommonAcmConfig = new AutoTestCommonAcmConfig();
        autotestCommonAcmConfig.setAcmClient(acmClient);
        autotestCommonAcmConfig.init();
        return autotestCommonAcmConfig;
    }

//    @Bean(initMethod = "init", name = "exhibitionAcmConfig")
//    public ExhibitionAcmConfig exhibitionAcmConfig() {
//        ExhibitionAcmConfig exhibitionAcmConfig = new ExhibitionAcmConfig();
//        return exhibitionAcmConfig;
//    }



}
