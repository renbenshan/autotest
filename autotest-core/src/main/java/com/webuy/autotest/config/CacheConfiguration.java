package com.webuy.autotest.config;

import com.webuy.cache.serializer.ISerializer;
import com.webuy.cache.serializer.KeySerialilzer;
import com.webuy.cache.serializer.StringSerializer;
import com.webuy.cache.springboot.stater.redis.SpringRedisCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;

/**
 * @author muze
 * @date 7/17/21 11:31 AM
 */
@Configuration
public class CacheConfiguration {


    /**
     * 配置 redis Cache manager
     * @param redisConnectionFactory redis 链接工厂
     * @param serializer 序列化
     * @param keySerializer key 序列话
     */
    @Bean
    public SpringRedisCacheManager springRedisCacheManager(RedisConnectionFactory redisConnectionFactory, ISerializer<Object> serializer, KeySerialilzer keySerializer){
        return new SpringRedisCacheManager(redisConnectionFactory,serializer,keySerializer);
    }


    @Bean
    public JacksonSerializer jacksonSerializer(){
        return new JacksonSerializer();
    }

    @Bean
    public StringSerializer stringSerializer(){
        return new StringSerializer();
    }


}
