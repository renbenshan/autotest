package com.webuy.autotest.DataSourceConstants;

/**
 * @author muze
 * @date 7/17/21 10:24 AM
 */
public interface DataSourceConstants {

    String MAPPER_PACKAGE = "com.webuy.autotest.dao";
    String MQ_BASE_MAPPER_PACKAGE = "com.aifocus.base.mq.dao";
    String MAPPER_LOCATION = "classpath:mapper/*.xml";
    String MQ_BASE_MAPPER_LOCATION = "classpath:baseMq/sqlmap/*.xml";

}