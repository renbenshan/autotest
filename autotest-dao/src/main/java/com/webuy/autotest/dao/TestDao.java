package com.webuy.autotest.dao;

import com.webuy.autotest.DO.TestDO;
import com.webuy.autotest.query.TestQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author muze
 * @date 7/17/21 10:29 AM
 */
@Repository
public interface TestDao {

    /**
     * 测试
     * @param testQuery 查询条件
     * @return list
     */
    List<TestDO> selectTest(TestQuery testQuery);

}