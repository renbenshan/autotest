package com.webuy.autotest.dao.exhibition;

import com.webuy.autotest.DO.WxhcExhibitionParkDO;
import com.webuy.autotest.query.ExhibitionParkQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author muze
 * @date 8/12/21 4:45 PM
 */

@Mapper
public interface ExhibitionParkDAO {

    List<WxhcExhibitionParkDO> queryExhibitionParkList(ExhibitionParkQuery exhibitionParkQuery);



}
