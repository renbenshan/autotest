package com.webuy.autotest.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author muze
 * @date 7/17/21 10:14 AM
 */
@Data
@Component
@ConfigurationProperties(prefix = "autotest.database")
public class AutoTestDataSourceProperty {

    /**
     * 数据库地址
     */
    private String url;

    /**
     * 账号
     */
    private String name;

    /**
     * 密码
     */
    private String passWord;
}
