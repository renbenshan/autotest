package com.webuy.autotest.config;

import com.aifocus.base.encryption.PasswordCallbackFromAcm;
import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.pool.DruidDataSource;
import com.webuy.autotest.DataSourceConstants.DataSourceConstants;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.transaction.TransactionDefinition.ISOLATION_DEFAULT;

/**
 * @author muze
 * @date 7/17/21 10:12 AM
 */
@Configuration
@MapperScan(basePackages = {DataSourceConstants.MAPPER_PACKAGE, DataSourceConstants.MQ_BASE_MAPPER_PACKAGE}, sqlSessionFactoryRef = "autotestSqlSessionFactory")
public class DataSourceConfig {


    @Resource
    private AutoTestDataSourceProperty autoTestDataSourceProperty;

    @Bean(name = "autotestDataSource", initMethod = "init")
    public DruidDataSource autotestDataSource(@Qualifier("autotestPasswordCallbackFromAcm") PasswordCallbackFromAcm passwordCallbackFromAcm) {

        DruidDataSource autotestDataSource = new DruidDataSource();
        autotestDataSource.setName("autotestDataSource");
        autotestDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        autotestDataSource.setUrl(autoTestDataSourceProperty.getUrl());
        autotestDataSource.setPasswordCallback(passwordCallbackFromAcm);
        autotestDataSource.setUsername(autoTestDataSourceProperty.getName());
        autotestDataSource.setMaxActive(20);
        autotestDataSource.setMinIdle(1);
        autotestDataSource.setMaxWait(5000);

        //慢sql的记录
        StatFilter statFilter = new StatFilter();
        ////sql合并
        statFilter.setMergeSql(true);
        statFilter.setSlowSqlMillis(30);
        statFilter.setLogSlowSql(true);
        Slf4jLogFilter slf4jLogFilter = new Slf4jLogFilter();
        slf4jLogFilter.setStatementExecutableSqlLogEnable(true);
        autotestDataSource.setProxyFilters(Arrays.asList(statFilter, slf4jLogFilter));
        return autotestDataSource;
    }


    /*********************
     *                   *
     *      事务源        *
     *                   *
     *********************/

    @Bean(name = "autotestTransactionTemplate")
    public TransactionTemplate whitewhaleTransactionTemplate(@Qualifier("autotestDataSource") DruidDataSource dataSource) {

        DataSourceTransactionManager transcationManager = new DataSourceTransactionManager(dataSource);
        TransactionTemplate transactionTemplate = new TransactionTemplate();
        transactionTemplate.setIsolationLevel(ISOLATION_DEFAULT);
        transactionTemplate.setTimeout(10);
        transactionTemplate.setTransactionManager(transcationManager);
        return transactionTemplate;
    }


    @Bean(name = "autotestSqlSessionFactory")
    public SqlSessionFactory mybatisSqlSessionFactory(@Qualifier("autotestDataSource") DataSource dataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setConfigLocation(new PathMatchingResourcePatternResolver().getResource("classpath:mybatis-config.xml"));


        org.springframework.core.io.Resource[] mqMapper = new PathMatchingResourcePatternResolver()
                .getResources(DataSourceConstants.MQ_BASE_MAPPER_LOCATION);


        org.springframework.core.io.Resource[] localDaoResources = new PathMatchingResourcePatternResolver()
                .getResources(DataSourceConstants.MAPPER_LOCATION);

        List<org.springframework.core.io.Resource> resourceList = new ArrayList<>(localDaoResources.length + mqMapper.length);
        resourceList.addAll(Arrays.asList(localDaoResources));
        resourceList.addAll(Arrays.asList(mqMapper));

        sessionFactory.setMapperLocations(resourceList.toArray(new org.springframework.core.io.Resource[4]));
        return sessionFactory.getObject();
    }

    /**
     * @see #autotestDataSource(PasswordCallbackFromAcm)
     * @return
     */
    @Bean(name = "autotestPasswordCallbackFromAcm")
    public PasswordCallbackFromAcm passwordCallbackFromAcm() {
        PasswordCallbackFromAcm jlimPasswordCallbackFromAcm = new PasswordCallbackFromAcm();
        jlimPasswordCallbackFromAcm.setPasswordInfoGroupId("datasource.password");
        jlimPasswordCallbackFromAcm.setPasswordInfoDataId("whitewhale");
        return jlimPasswordCallbackFromAcm;
    }

}
