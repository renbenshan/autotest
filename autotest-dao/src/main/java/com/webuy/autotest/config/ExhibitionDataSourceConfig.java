package com.webuy.autotest.config;

import com.aifocus.base.encryption.PasswordCallbackFromAcm;
import com.alibaba.druid.pool.DruidDataSource;
import lombok.Data;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import javax.security.auth.callback.PasswordCallback;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.transaction.TransactionDefinition.ISOLATION_DEFAULT;

/**
 * @author muze
 * @date 8/12/21 4:21 PM
 */

@Component
@ConfigurationProperties(prefix = "exhibition.datasource")
@MapperScan(basePackages = {"com.webuy.autotet.dao.exhibition", "com.aifocus.base.mq.dao"}, sqlSessionFactoryRef = "exhibitionSqlSessionFactory")
public class ExhibitionDataSourceConfig {

    @Value("${exhibition.datasource.jdbc.url}")
    private String url;

    @Value("${exhibition.datasource.username}")
    private String username;

    @Value("${exhibition.datasource.maxActive}")
    private Integer maxActive;

    @Value("${env}")
    private String env;


    @Bean(name = "exhibitionDataSource")
//    @DependsOn("exhibitionAcmConfig")
    public DataSource mybatisDataSource(@Qualifier("passwordCallback") PasswordCallback passwordCallback) {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPasswordCallback(passwordCallback);
        dataSource.setMaxActive(maxActive);
        dataSource.setMaxWait(5000);
        dataSource.setInitialSize(3);
        return dataSource;
    }

    @Bean(name = "passwordCallback")
    public PasswordCallback passwordCallback() {
        PasswordCallbackFromAcm passwordCallback = new PasswordCallbackFromAcm();
        passwordCallback.setPasswordInfoGroupId("datasource.password");
        passwordCallback.setPasswordInfoDataId("exhibition_platform");
        return passwordCallback;
    }

    @Bean(name = "exhibitionTransactionManager")
    public DataSourceTransactionManager mybatisTransactionManager(@Qualifier("exhibitionDataSource") DataSource mybatisDataSource) {
        return new DataSourceTransactionManager(mybatisDataSource);
    }

    @Bean(name = "exhibitionSqlSessionFactory")
    public SqlSessionFactory mybatisSqlSessionFactory(@Qualifier("exhibitionDataSource") DataSource mybatisDataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(mybatisDataSource);
        String locationPattern = "classpath:mapper/exhibition/*.xml";
        Resource[] localMapperResources = new PathMatchingResourcePatternResolver()
                .getResources(locationPattern);

        String mqMapperStr = "classpath*:baseMq/sqlmap/*.xml";
        Resource[] mqMapper = new PathMatchingResourcePatternResolver()
                .getResources(mqMapperStr);


        List<Resource> resourceList = new ArrayList<>(localMapperResources.length + mqMapper.length);
        resourceList.addAll(Arrays.asList(localMapperResources));
        resourceList.addAll(Arrays.asList(mqMapper));

        Resource[] resources = new Resource[resourceList.size()];
        sessionFactory.setMapperLocations(resourceList.toArray(resources));


        return sessionFactory.getObject();
    }

    @Bean(name = "exhibitionTransactionTemplate")
    public TransactionTemplate transactionTemplate(@Qualifier("exhibitionTransactionManager") DataSourceTransactionManager mybatisTransactionManager) {
        TransactionTemplate transactionTemplate = new TransactionTemplate();
        transactionTemplate.setIsolationLevel(ISOLATION_DEFAULT);
        transactionTemplate.setTimeout(10);
        transactionTemplate.setTransactionManager(mybatisTransactionManager);
        return transactionTemplate;
    }

}
