package com.webuy.autotest.DO;

import com.webuy.autotest.dto.TestDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.Collections;

/**
 * @author muze
 * @date 7/17/21 10:29 AM
 */
@Data
public class TestDO implements Serializable {

    private static final long serialVersionUID = 456220458334280844L;


    private String value;


    public static TestDTO convert(TestDO DO){
        if (DO == null){
            return null;
        }
        return TestDTO
                .builder()
                .value(Collections.singletonList(DO.value))
                .build();
    }

}