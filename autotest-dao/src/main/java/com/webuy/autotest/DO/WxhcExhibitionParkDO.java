package com.webuy.autotest.DO;

import com.webuy.exhibitionplatform.enums.EnumExhibitionParkOutChannelSourceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author muze
 * @date 8/12/21 4:56 PM
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxhcExhibitionParkDO {
    /**
     * 主键id
     */
    private Long exhibitionParkId;

    /**
     * 会场名
     */
    private String exhibitionParkName;

    /**
     * 会场类型，线上／线下
     */
    private Integer exhibitionParkType;

    /**
     * 页面配置json
     */
    private String pageConfigJson;

    /**
     * 会场开始时间
     */
    private Date gmtStart;

    /**
     * 会场结束时间
     */
    private Date gmtEnd;

    /**
     * 会场扩展限制属性
     */
    private String exhibitionParkLimitJson;

    /**
     * 会场扩展规则属性
     */
    private String exhibitionParkRuleJson;

    /**
     * 更新时间
     */
    private Date gmtModify;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 逻辑删除
     */
    private Integer isDelete;

    /**
     * 是否是永久会场
     */
    private Boolean forever;

    /**
     * 店铺id，数据权限区分字段
     */
    private Long shopId;

    /**
     * 业务类型
     */
    private Long bizType;

    /**
     * 业务子类型
     */
    private Long subBizType;

    /**
     * 类目id
     */
    private Long categoryId;

    /**
     * 营销类目id
     */
    private Long marketCategoryId;

    /**
     * 会场创建人ID
     */
    private Long creatorId;

    /**
     * 创建人类型
     */
    private Integer creatorType;

    /**
     * 会场行为状态
     */
    private Integer exhibitionParkStatus;

    /**
     * 会场审核状态
     */
    private Integer applyStatus;

    /**
     * 会场排序
     */
    private Integer exhibitionParkSort;

    /**
     * 甩货发布字段
     */
    private Integer shRelease;

    /**
     * 芝麻仓发布字段
     */
    private Integer scsRelease;

    private Integer ownerType;
    private Long ownerId;
    /**
     * 业务方自己定义的会场类型
     */
    private Integer exhibitionBizType;
    /**
     * 定时发布时间
     */
    private Date gmtTimingRelease;

    /**
     * 预热时间
     */
    private Date gmtWarm;
    private Integer attributeCc;

    /**
     * 会场标签
     */
    private Integer exhibitionFlag;

    /**
     * 会场开售时间
     */
    private Date gmtOnline;

    /**
     * 会场时间状态
     */
    private Integer timeStatus;
    /**
     * 测试场景标记
     */
    private Integer testScene;

    /**
     * 来源业务子类型
     */
    private Long sourceSubBizType;

    /**
     * 外部渠道类型
     * @see EnumExhibitionParkOutChannelSourceType
     */
    private Integer outChannelSourceType;

    /**
     * 标签
     */
    private String tags;
}
