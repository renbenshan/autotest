package com.webuy.autotest.query;

import com.webuy.exhibitionplatform.enums.EnumActionStatus;
import com.webuy.exhibitionplatform.enums.EnumExhibitionParkOutChannelSourceType;
import com.webuy.exhibitionplatform.enums.EnumExhibitionParkStatus;
import com.webuy.exhibitionplatform.enums.EnumExhibitionParkType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author muze
 * @date 8/12/21 4:55 PM
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExhibitionParkQuery {
    /**
     * 会场ID列表
     */
    private List<Long> exhibitionParkIdList;

    /**
     * 会场名
     * 索引字段
     */
    private String exhibitionParkName;

    /**
     * 会场类型
     * @see EnumExhibitionParkType
     */
    private Integer exhibitionParkType;

    /**
     * 会场类型列表
     * @see EnumExhibitionParkType
     */
    private List<Integer> exhibitionParkTypeList;

    /**
     * 会场状态列表
     * @see EnumActionStatus
     */
    private List<Integer> exhibitionParkStatusList;

    /**
     * 开始时间前
     */
    @Deprecated
    private Date gmtStartBegin;
    /**
     * 同 gmtStartBegin  开始时间 >= 该值
     */
    private Date gmtStartGreateThanOrEqual;

    /**
     * 开始时间后
     */
    @Deprecated
    private Date gmtStartAfter;
    /**
     * 同 gmtStartAfter 开始时间 < 该值
     */
    private Date gmtStartLessThan;

    /**
     * 结束时间前
     */
    @Deprecated
    private Date gmtEndBegin;
    /**
     * 结束时间 >= 该值
     */
    private Date gmtEndGreateThanOrEqual;

    /**
     * 结束时间后
     */
    @Deprecated
    private Date gmtEndAfter;
    /**
     * 结束时间 < 该值
     */
    private Date gmtEndLessThan;


    /**
     * 逻辑删除
     */
    private Integer isDelete;

    /**
     * 是否是永久会场
     */
    private Boolean forever;

    /**
     * 店铺id，数据权限区分字段
     * 索引字段
     */
    private Long shopId;

    /**
     * 业务类型
     */
    private Long bizType;

    /**
     * 业务子类型
     */
    private Long subBizType;
    private List<Long> subBizTypes;


    /**
     * 类目id
     * 索引字段
     */
    private Long categoryId;

    /**
     * 营销类目id
     * 索引字段
     */
    private Long marketCategoryId;
    private List<Long> marketCategoryIds;


    /**
     * 创建人类型
     */
    private Integer creatorType;

    /**
     * 会场状态
     * @see EnumExhibitionParkStatus
     * 索引字段
     */
    private Integer exhibitionParkStatus;

    /**
     * 会场审核状态
     * @see com.webuy.exhibitionplatform.enums.EnumExhibitionParkApplyStatus
     *
     */
    private Integer applyStatus;

    /**
     * 会场排序code
     */
    private Integer exhibitionParkSortCode;

    /**
     * 会场排序值
     */
    private String exhibitionParkSortValue;

    private Integer ownerType;
    private Long ownerId;
    /**
     * 业务方自己定义的会场类型
     */
    private Integer exhibitionBizType;
    /**
     * 定时发布时间
     */
    private Date gmtTimingRelease;

    /**
     * 预热时间
     */
    private Date gmtWarm;
    private Integer attributeCc;

    /**
     * 会场创建人ID
     */
    private Long creatorId;

    /**
     * 会场创建人ID
     */
    private List<Long> creatorIdList;

    /**
     * 开售时间
     */
    private Date gmtOnline;

    /**
     * 开售时间开始
     */
    private Date gmtOnlineBegin;

    /**
     * 开售时间结束
     */
    private Date gmtOnlineEnd;

    /**
     * 会场标签
     */
    private Integer exhibitionFlag;

    /**
     * 供应商ID集合
     */
    private List<Long> ownerIdList;

    /**
     * 时间状态
     *
     * @see com.webuy.exhibitionplatform.enums.EnumTimeStatus
     */
    private List<Integer> timeStatusList;

    /**
     * 会场时间状态 （必填）
     * @see com.webuy.exhibitionplatform.enums.EnumTimeStatus
     */
    private Integer timeStatus;

    /**
     * 从扩展字段里筛选，为了解决 推荐供应商会场问题
     */
    private Long brandId;

    /**
     *  如果只传   minExhibitionParkSort  exhibitionParkSort >= minExhibitionParkSort
     *  如果只传   maxExhibitionParkSort  exhibitionParkSort <= maxExhibitionParkSort
     *  连个值都传  minExhibitionParkSort <= exhibitionParkSort <= maxExhibitionParkSort
     */
    private Integer minExhibitionParkSort;
    private Integer maxExhibitionParkSort;


    /**
     * 来源业务子类型
     */
    private Long sourceSubBizType;


    /**
     * 外部渠道类型
     * @see EnumExhibitionParkOutChannelSourceType
     */
    private Integer outChannelSourceType;


    /**
     * tag
     * @see com.webuy.exhibitionplatform.enums.EnumTagType
     */
    private String tags;
}
