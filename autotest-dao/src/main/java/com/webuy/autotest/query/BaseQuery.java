package com.webuy.autotest.query;

/**
 * @author muze
 * @date 7/17/21 10:30 AM
 */
public abstract class BaseQuery{

    /**
     * 页码
     */
    protected Integer pageNo = 1;

    /**
     * 每页个数
     */
    protected Integer pageSize = 10;
    /**
     * 偏移量
     */
    private Integer offset;

    /**
     * 分组sql
     */
    private String groupBySql;

    /**
     * 排序sql
     */
    private String orderBySql;

    /**
     * 字段
     */
    private String field;

    /**
     * 排序方式
     */
    private String sortType;

    /**
     * 分页数据偏移量
     */
    public Integer getOffset() {
        computeOffset();
        return offset;
    }

    /**
     * 计算分页偏移量
     */
    public Integer computeOffset(){
        if (null == pageNo || null == pageSize) {
            return null;
        }
        offset = (pageNo - 1) * pageSize;
        return offset;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        if (pageNo != null) {
            this.pageNo = pageNo;
        }
        computeOffset();
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        if (pageSize != null) {
            this.pageSize = pageSize;
        }
        computeOffset();
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getOrderBySql() {
        return orderBySql;
    }

    public void setOrderBySql(String orderBySql) {
        this.orderBySql = orderBySql;
    }

    public String getGroupBySql() {
        return groupBySql;
    }

    public void setGroupBySql(String groupBySql) {
        this.groupBySql = groupBySql;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }
}
