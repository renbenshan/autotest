package com.webuy.autotest.query;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * @author muze
 * @date 7/17/21 10:30 AM
 */
@Data
@Builder
public class TestQuery extends BaseQuery {


    private Date gmtStart;


}
