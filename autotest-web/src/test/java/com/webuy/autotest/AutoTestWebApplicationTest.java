package com.webuy.autotest;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * @author muze
 * @date 7/19/21 12:28 AM
 */
@SpringBootTest
@SpringBootApplication
@ComponentScan(value = {"com.aifocus.base.mq","com.webuy.autotest"})
@DubboComponentScan(basePackages = "com.webuy.autotest.apiImpl.impl")
// ,"classpath:spring/stock-client.xml"
@ImportResource(locations = {"classpath:spring/itemplatform-client.xml"})
@PropertySource(value = {"classpath:application.properties"})
public class AutoTestWebApplicationTest {
    @Test
    void contextLoads() {
    }


    @Test
    public void test(){
        System.out.println("<<<<<测试");
    }

}