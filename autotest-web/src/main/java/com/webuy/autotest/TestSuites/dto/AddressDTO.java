package com.webuy.autotest.TestSuites.dto;

import com.webuy.buy.enums.SubPlatformEnum;
import com.webuy.buy.enums.WxhcPlatformEnum;

/**
 * @author muze
 * @date 8/12/21 2:25 PM
 */
public class AddressDTO {

    /**
     * 好衣库的：用户ID
     */
    public static final Long HYK_CUSER_ID =2222243053556L;


    /**
     * 好衣库的：购买人ID
     */

    public static final String BUYER_ID ="1000000000123624";

    /**
     * 好衣库购买对应的手机号
     */

    public static final String MOBILE ="18756939317";


    /**
     * 公共的地址信息，好衣库，蜂享家等业务均可用
     */

    public static final String PART_ADRESS ="自动化测试地址";

    public static final String ADRESS ="北京市北京市市辖区东城区测试";

    public static final String APPID ="wxff65b8f3a9650f25";

    public static final Integer AREA_CODE =110101;

    public static final Integer CITY_CODE =110100;

    public static final Integer PROVINCE_CODE =110000;

    public static final Long DELIVER_ADDRESS_ID =1067577L;

    public static final String RECEIVER_NAME ="沐泽自动化测试";

    public static final String RECEIVER_MOBILE ="18756939317";

    /**
     * 好衣库：账户绑定的微信相关信息
     */

    public static final String OPEN_ID ="oDLX_1DUgmdWeZa0iuODReoKmNAw";

    public static final String UNION_ID ="oEdTzwVpZpcDTh-qhAiA1b4hBfRc";

    /**
     * 好衣库：from 类型
     * 平台类型，见枚举{@link WxhcPlatformEnum}
     *  子平台类型，见枚举{@link SubPlatformEnum}
     */

    public static final String HYK_FROM = "3";

    public static final  Integer HYK_ReqPlatform =2;

    public static final  Integer HYK_SubReqPlatform =3;


    /**
     * 蜂享家：用户ID
     * 平台类型，见枚举{@link WxhcPlatformEnum}
     * 不需要传 SubPlatformEnum 类型
     */
    public static  final Long FXJ_CUSER_ID =22222429685L;

    public static final String FXJ_FROM = "26";

    public static final  Integer FXJ_ReqPlatform =26;


    /**
     * 蜂享家：购买人ID
     * 不需要 这个SubReqplatform
     */

    public static final String FXJ_BUYER_ID ="22222429685";





}
