package com.webuy.autotest.TestSuites;

import com.aifocus.base.common.result.Result;
import com.aifocus.itemplatform.api.PitemApiService;
import com.aifocus.itemplatform.api.client.ItemClient;
import com.aifocus.itemplatform.dto.WxhcItemDTO;
import com.webuy.autotest.TestSuites.dto.AddressDTO;
import com.webuy.autotest.TestSuites.enums.EnumSubBizType;
import com.webuy.autotest.base.BaseTest;
import com.webuy.buy.api.PayOrderApi;
import com.webuy.buy.api.PayServiceApi;
import com.webuy.buy.api.order.SubBizOrderApi;
import com.webuy.buy.dto.ItemInfoDTO;
import com.webuy.buy.dto.PayResultDTO;
import com.webuy.buy.dto.WxhcSubmitOrderDTO;
import com.webuy.buy.dto.order.WxhcSubBizOrderClientDTO;
import com.webuy.buy.dto.query.WxhcSubBizOrderQuery;
import com.webuy.exhibitionplatform.restructure.api.ExhibitionParkFacadeApi;
import com.webuy.exhibitionplatform.restructure.dto.ExhibitionParkApiDTO;
import com.webuy.exhibitionplatform.restructure.query.ExhibitionParkFacadeQueryDTO;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author muze
 * @date 7/20/21 11:23 AM
 */

public class TestNormalExhibitionPay extends BaseTest{



    @Autowired
    private ExhibitionParkFacadeApi exhibitionParkFacadeApi;

    @Autowired
    private ItemClient itemClient;
    @Autowired
    private PitemApiService pitemApiService;
    @Autowired
    private PayOrderApi payOrderApi;
    @Autowired
    private PayServiceApi payServiceApi;
    @Autowired
    private SubBizOrderApi subBizOrderApi;

    @Test(testName = "获取线上普通会场")
    public void queryExhibition() throws ParseException {
        this.testQyeryExhibitionId();
    }

    @Test( dataProvider = "queryExhibitionId")
    public void queryPitemAndPay(Long exhibitionId)  {

        Result<List<Long>>  result =pitemApiService.queryPitemIdsByExhibitionParkId(exhibitionId);
        List<Long> list =result.getEntry();
        List<WxhcItemDTO> WxhcItemDTOList=itemClient.getItemByPItemId(list.get(0));

        for (WxhcItemDTO wxhcItemDTO:WxhcItemDTOList
             ) {
            Long itemId =wxhcItemDTO.getId();
            logger.info("会场ID:"+exhibitionId+"会场下的itemId:"+itemId);

            WxhcSubmitOrderDTO wxhcSubmitOrder =new WxhcSubmitOrderDTO();

            ItemInfoDTO itemInfoDTO =new ItemInfoDTO();
            itemInfoDTO.setItemId(itemId);
            itemInfoDTO.setItemNum(1);
            itemInfoDTO.setExhibitionId(exhibitionId);
            itemInfoDTO.setPitemId(wxhcItemDTO.getWxhcPitemId());
            List<ItemInfoDTO> itemList =new ArrayList<>();
            itemList.add(itemInfoDTO);
            wxhcSubmitOrder.setItemList(itemList);
            wxhcSubmitOrder.setCuserId(AddressDTO.HYK_CUSER_ID);
            wxhcSubmitOrder.setBuyerId(AddressDTO.BUYER_ID);
            wxhcSubmitOrder.setMobile(AddressDTO.MOBILE);
            wxhcSubmitOrder.setAddress(AddressDTO.ADRESS);
            wxhcSubmitOrder.setPartAddress(AddressDTO.PART_ADRESS);
            wxhcSubmitOrder.setAppId(AddressDTO.APPID);
            wxhcSubmitOrder.setAreaCode(AddressDTO.AREA_CODE);
            wxhcSubmitOrder.setCityCode(AddressDTO.CITY_CODE);
            wxhcSubmitOrder.setProvinceCode(AddressDTO.PROVINCE_CODE);
            wxhcSubmitOrder.setRealName("沐泽");
            wxhcSubmitOrder.setExhibitionId(exhibitionId);
//            wxhcSubmitOrder.setPayTradeSeparateType(2);
            wxhcSubmitOrder.setDeliverAddressId(AddressDTO.DELIVER_ADDRESS_ID);
            wxhcSubmitOrder.setReceiverName(AddressDTO.RECEIVER_NAME);
            wxhcSubmitOrder.setReceiverMobile(AddressDTO.RECEIVER_MOBILE);
            wxhcSubmitOrder.setOpenId(AddressDTO.OPEN_ID);
            wxhcSubmitOrder.setUnionId(AddressDTO.UNION_ID);
            wxhcSubmitOrder.setTotalPostage(0D);
            wxhcSubmitOrder.setSubBizType(EnumSubBizType.HAOYIKU.getCode());
            wxhcSubmitOrder.setOrderType(1);
            wxhcSubmitOrder.setSubOrderType(1);
            wxhcSubmitOrder.setOpenGId("");
//            wxhcSubmitOrder.setPayOrderOpenId(wxhcSubmitOrder.getOpenId());
            wxhcSubmitOrder.setCrossType(1);
            wxhcSubmitOrder.setFrom(AddressDTO.HYK_FROM);
            wxhcSubmitOrder.setReqPlatform(AddressDTO.HYK_ReqPlatform);
            wxhcSubmitOrder.setSubReqplatform(AddressDTO.HYK_SubReqPlatform);
            wxhcSubmitOrder.setHykSettlementFrom(1);
            /**
             *  结算
             *
             */

            com.webuy.buy.common.result.Result buyResult=payOrderApi.settlement(wxhcSubmitOrder);


            JSONArray js = JSONArray.fromObject(wxhcSubmitOrder);
            String array = js.toString();
            Object result2 =buyResult.getEntry();
            JSONObject jsonObject = new JSONObject();
            jsonObject =jsonObject.fromObject(result2);
            Long totalPayPrice = Long.valueOf(jsonObject.getString("totalPayPrice"));
            String pageToken  = jsonObject.getString("pageToken");

            /**
             * 支付
             */
            wxhcSubmitOrder.setWxPayType(1);
            wxhcSubmitOrder.setAccountPayPrice(totalPayPrice);
            //为了防止重复调用
            wxhcSubmitOrder.setToken(pageToken);
            wxhcSubmitOrder.setBuyerMobile("18756939317");
            com.webuy.buy.common.result.Result<PayResultDTO> buy =payOrderApi.buy(wxhcSubmitOrder);

            PayResultDTO payResultDTO=buy.getEntry();
            Assert.assertNotNull(buy.getEntry().getBizOrderId());
            logger.info("生成的订单ID："+buy.getEntry().getBizOrderId());



            WxhcSubBizOrderQuery subBizOrderQuery =new WxhcSubBizOrderQuery();
            subBizOrderQuery.setCUserId(AddressDTO.HYK_CUSER_ID);

            subBizOrderQuery.setBizOrderId(buy.getEntry().getBizOrderId());
            Result<List<WxhcSubBizOrderClientDTO>> orderResult=subBizOrderApi.queryWithCUserId(subBizOrderQuery);
            List<WxhcSubBizOrderClientDTO> subOrderList =orderResult.getEntry();
            for (WxhcSubBizOrderClientDTO wxhcSubBizOrderClientDTO:subOrderList
                 ) {
                logger.info("结算价:"+wxhcSubBizOrderClientDTO.getSettlementPrice()+
                        "供货价:"+wxhcItemDTO.getSupplierPrice()+
                        "itemSalePrice"+wxhcSubBizOrderClientDTO.getItemSalePrice()+
                        "总价格："+wxhcSubBizOrderClientDTO.getTotalPrice()+
                        "SKu销售价："+wxhcSubBizOrderClientDTO.getSkuSalePrice()+
                        "账户支付价："+wxhcSubBizOrderClientDTO.getAccountPayPrice());
                logger.info("供货价:"+wxhcItemDTO.getSupplierPrice()+" "+"B端售价:"+wxhcItemDTO.getPrice()+" "+"结算价:"+wxhcItemDTO.getSettlementPrice()+
                        "TaoBaoPrice:"+wxhcItemDTO.getTaoBaoPrice());
                Assert.assertEquals(wxhcItemDTO.getSupplierPrice(),wxhcSubBizOrderClientDTO.getSupplierPrice());
                Assert.assertEquals(wxhcItemDTO.getSettlementPrice(),wxhcSubBizOrderClientDTO.getSettlementPrice());
//                Assert.assertEquals(wxhcItemDTO.getSupplierPrice(),wxhcSubBizOrderClientDTO.getAccountPayPrice());
                Assert.assertEquals(wxhcSubBizOrderClientDTO.getTotalPrice(),wxhcSubBizOrderClientDTO.getAccountPayPrice());
            }

            //只有第三方支付的时候，才会进行这个接口的调用
//            ContinuePayRequest request =new ContinuePayRequest();
//            request.setPayType(2);
//            request.setBizOrderId(buy.getEntry().getBizOrderId());
//            List<String> bizOrderIdList =new ArrayList<>();
//            bizOrderIdList.add(buy.getEntry().getBizOrderId());
//            request.setBizOrderIdList(bizOrderIdList);
//            request.setCuserId(2222243053556L);
//            com.webuy.buy.common.result.Result<PayResultDTO> PayResultDTO =payServiceApi.continuePay(request);
//            System.out.println(PayResultDTO);
        }



    }

    @DataProvider( name ="queryExhibitionId" )
    public Object[][] testQyeryExhibitionId() throws ParseException {


        Object[][] objects =null;
//        if(method.getName() =="getNormalExhibition"){
            ExhibitionParkFacadeQueryDTO exhibitionParkFacadeQueryDTO =new ExhibitionParkFacadeQueryDTO();
            exhibitionParkFacadeQueryDTO.setSubBizType(301L);

            //线上类型
            exhibitionParkFacadeQueryDTO.setExhibitionParkType(1);
            //timeStatus   时间状态
            List<Integer> list =new ArrayList<>();
            list.add(2);
            exhibitionParkFacadeQueryDTO.setTimeStatusList(list);

            //actionStatus 活动状态
            List<Integer> exhibitionParkStatusList =new ArrayList<>();
            exhibitionParkStatusList.add(102);
            exhibitionParkFacadeQueryDTO.setExhibitionParkStatusList(exhibitionParkStatusList);

            Result<List<ExhibitionParkApiDTO>> entry= exhibitionParkFacadeApi
                    .queryExhibitionParkList(exhibitionParkFacadeQueryDTO);
            List<ExhibitionParkApiDTO> exhibitionParkApiDTOList =entry.getEntry();
                List<ExhibitionParkApiDTO> newExhibitonList = new ArrayList<>();
                for (ExhibitionParkApiDTO exhibitionParkApiDTO: exhibitionParkApiDTOList
                ) {

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                    String date = simpleDateFormat.format(new Date());
                    Date time= simpleDateFormat.parse(date);

                    //getStatus:4 线上进行中  tag:100 普通会场
                    if(exhibitionParkApiDTO.getStatus() ==4 && exhibitionParkApiDTO.getGmtEnd().after(time)){
                        newExhibitonList.add(exhibitionParkApiDTO);
                        logger.info("会场ID："+exhibitionParkApiDTO.getExhibitionParkId()+"会场的起止时间"+exhibitionParkApiDTO.getGmtStart().getTime()+"~"+exhibitionParkApiDTO.getGmtEnd().getTime());
                        Random random = new Random();

                        if(newExhibitonList.size()<=0){
                            logger.info("满足类型的会场数据为空");
                        }
                        Long ExhibitionId =exhibitionParkApiDTO.getExhibitionParkId();
                        logger.info("随机取的对象是："+exhibitionParkApiDTO.getExhibitionParkId());
                        objects =new Object[][] { {ExhibitionId} };
                        return objects;
                    }
                }
                return objects;
    }

}
