package com.webuy.autotest.TestSuites;

import com.aifocus.base.common.result.Result;
import com.aifocus.itemplatform.api.PitemApiService;
import com.aifocus.itemplatform.api.SpuApiService;
import com.aifocus.itemplatform.api.WxhcPitemUploadApi;
import com.aifocus.itemplatform.api.client.ItemClient;
import com.aifocus.itemplatform.common.enums.EnumChannel;
import com.aifocus.itemplatform.common.enums.EnumPitemStatus;
import com.aifocus.itemplatform.dto.WxhcItemDTO;
import com.aifocus.itemplatform.dto.WxhcSpuDTO;
import com.aifocus.itemplatform.dto.pitem.CreatePitemParams;
import com.webuy.autotest.TestSuites.dto.AddressDTO;
import com.webuy.autotest.TestSuites.enums.EnumSubBizType;
import com.webuy.autotest.apiImpl.PromotionApi;
import com.webuy.autotest.dto.CreatePromotionDTO;
import com.webuy.buy.api.PayOrderApi;
import com.webuy.buy.api.order.SubBizOrderApi;
import com.webuy.buy.dto.ItemInfoDTO;
import com.webuy.buy.dto.PayResultDTO;
import com.webuy.buy.dto.WxhcSubmitOrderDTO;
import com.webuy.buy.dto.order.WxhcSubBizOrderClientDTO;
import com.webuy.buy.dto.query.WxhcSubBizOrderQuery;
import com.webuy.exhibitionplatform.request.ReleaseExhibitionParkRequest;
import com.webuy.exhibitionplatform.restructure.api.ExhibitionParkInnerFacadeApi;
import com.webuy.promotion.api.vo.Promotion;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import com.webuy.autotest.apiImpl.ExhibiionInActivityApi;
import com.webuy.autotest.apiImpl.ExhibitionApi;
import com.webuy.autotest.apiImpl.ExhibitionPitemApiImpl;
import com.webuy.autotest.base.BaseTest;
import com.webuy.exhibitionplatform.enums.EnumExhibitionParkType;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class TestAllAndPay extends BaseTest {

    @Autowired
    private ExhibiionInActivityApi exhibiionInActivityApi;

    @Autowired
    private ExhibitionPitemApiImpl exhibitionPitemApi;
    @Autowired
    private WxhcPitemUploadApi wxhcPitemUploadApi;

    @Autowired
    private ExhibitionParkInnerFacadeApi exhibitionParkInnerFacadeApi;
    @Autowired
    private ItemClient itemClient;

    @Autowired
    private PayOrderApi payOrderApi;

    @Autowired
    private PromotionApi promotionApi;

    @Test(testName = "创建促销会场")
    public void testCreatePromotionByExhibitionId() throws ParseException{
        Long subBizType =301L;//
        Integer exhibitionParkType = EnumExhibitionParkType.ONLINE.getCode();
        HashMap exhibitionMessage = createExhibitionAndActivity();
        Long exhibitionId = Long.valueOf(exhibitionMessage.get("exhibitionId").toString());
        Long activityId = Long.valueOf(exhibitionMessage.get("activityId").toString());
        Result<List<Long>> result = addSpuToExhibition(subBizType,exhibitionParkType,exhibitionId,activityId);
        List<Long> pItemIdList = result.getEntry();

        CreatePromotionDTO createPromotionDTO =new CreatePromotionDTO();
        createPromotionDTO.setTitle("自动创建的一口价");
        createPromotionDTO.setBusinessCode("1001");//n元n件
        createPromotionDTO.setPromotionType("1002");//好衣库
        createPromotionDTO.setActionValue("5000");//好衣库价格
        createPromotionDTO.setBackActionValue("6000");//云店价格
        createPromotionDTO.setConditionValue("1");//一件
        createPromotionDTO.setExhibitionId(exhibitionId);
        createPromotionDTO.setUnderTake(1);//商家承担

        com.webuy.promotion.api.vo.Result<Promotion> pResult =promotionApi.createPromotion(createPromotionDTO);
        Promotion promotionResult =pResult.getEntry();

        List<String> list = calculatePromotion(exhibitionId,pItemIdList);


    }


    //会场创建、活动创建
    public HashMap createExhibitionAndActivity() throws ParseException{
        HashMap exhibition = new HashMap();

        com.aifocus.base.common.result.Result activityResult=exhibiionInActivityApi.addActivity();
        Object json =activityResult.getEntry();
        JSONObject data = JSONObject.fromObject(json);
        Object newEntry =data.getJSONObject("entry");
        JSONObject jsonObject = JSONObject.fromObject(newEntry);
        String activityId= jsonObject.get("saleActivityId").toString();
        String exhibitionId= jsonObject.get("exhibitionParkId").toString();
        exhibition.put("exhibitionId",exhibitionId);
        exhibition.put("activityId",activityId);

        return exhibition;
    }

    //添加商品
    public Result<List<Long>> addSpuToExhibition(Long subBizType,Integer exhibitionParkType,Long exhibitionId,Long activityId) throws ParseException{
        if(subBizType ==null&&exhibitionParkType==null){
            throw new RuntimeException("参数类型有误");
        }
        CreatePitemParams createPItemParams =new CreatePitemParams();
        com.aifocus.itemplatform.common.result.Result result =exhibitionPitemApi.queryPitemList();
        List<WxhcSpuDTO> list = (List<WxhcSpuDTO>) result.getEntry();
        List<Long> spuIdList = new ArrayList<>();
        for (WxhcSpuDTO wxhcSpuDTO:list){
            Long spuId = wxhcSpuDTO.getId();
            spuIdList.add(spuId);
        }
        createPItemParams.setSpuIdList(spuIdList);
        createPItemParams.setSubBizType(subBizType);
        createPItemParams.setTargetId(Long.valueOf(activityId));
        createPItemParams.setPitemStatus(EnumPitemStatus.ONLINE);
        if (subBizType == 301l){
            createPItemParams.setEnumChannel(EnumChannel.HYK);
        }else {
            createPItemParams.setEnumChannel(EnumChannel.BEE_HOME);
        }
        com.aifocus.base.common.result.Result<List<Long>> pitemResult=wxhcPitemUploadApi.createExhibitionPitem(subBizType,exhibitionParkType,createPItemParams);
        ReleaseExhibitionParkRequest releaseExhibitionParkRequest=new ReleaseExhibitionParkRequest();
        releaseExhibitionParkRequest.setExhibitionParkId(Long.valueOf(exhibitionId));
        com.aifocus.base.common.result.Result<Void> voidResult = exhibitionParkInnerFacadeApi.releaseExhibitionPark(releaseExhibitionParkRequest);

        return pitemResult;
    }

    //计算促销价格
    public List<String> calculatePromotion(Long exhibitionId,List<Long> pitemList){
//        Result<List<Long>>  result =pitemApiService.queryPitemIdsByExhibitionParkId(exhibitionId);
//        List<Long> list =result.getEntry();//pitemId

        List<WxhcItemDTO> WxhcItemDTOList=itemClient.getItemByPItemId(pitemList.get(0));
        List<String> bizOrder = new ArrayList<>();
        for (WxhcItemDTO wxhcItemDTO:WxhcItemDTOList
        ) {
            Long itemId = wxhcItemDTO.getId();
            //logger.info("会场下的itemId:" + itemId + "结算价格:" + wxhcItemDTO.getSettlementPrice() + "" + "B端价格:" + wxhcItemDTO.getPrice());
            wxhcItemDTO.getSupplierPrice();
            wxhcItemDTO.getPrice();
            wxhcItemDTO.getSettlementPrice();
            wxhcItemDTO.getSupplierPrice();
            WxhcSubmitOrderDTO wxhcSubmitOrder = new WxhcSubmitOrderDTO();

            ItemInfoDTO itemInfoDTO = new ItemInfoDTO();
            itemInfoDTO.setItemId(itemId);
            itemInfoDTO.setItemNum(1);
            itemInfoDTO.setExhibitionId(exhibitionId);
            itemInfoDTO.setPitemId(wxhcItemDTO.getWxhcPitemId());
            List<ItemInfoDTO> itemList = new ArrayList<>();
            itemList.add(itemInfoDTO);
            wxhcSubmitOrder.setItemList(itemList);
            wxhcSubmitOrder.setCuserId(AddressDTO.HYK_CUSER_ID);
            wxhcSubmitOrder.setBuyerId(AddressDTO.BUYER_ID);
            wxhcSubmitOrder.setMobile(AddressDTO.MOBILE);
            wxhcSubmitOrder.setAddress(AddressDTO.ADRESS);
            wxhcSubmitOrder.setPartAddress(AddressDTO.PART_ADRESS);
            wxhcSubmitOrder.setAppId(AddressDTO.APPID);
            wxhcSubmitOrder.setAreaCode(AddressDTO.AREA_CODE);
            wxhcSubmitOrder.setCityCode(AddressDTO.CITY_CODE);
            wxhcSubmitOrder.setProvinceCode(AddressDTO.PROVINCE_CODE);
            wxhcSubmitOrder.setExhibitionId(exhibitionId);
            wxhcSubmitOrder.setDeliverAddressId(AddressDTO.DELIVER_ADDRESS_ID);
            wxhcSubmitOrder.setReceiverName(AddressDTO.RECEIVER_NAME);
            wxhcSubmitOrder.setReceiverMobile(AddressDTO.RECEIVER_MOBILE);
            wxhcSubmitOrder.setOpenId(AddressDTO.OPEN_ID);
            wxhcSubmitOrder.setUnionId(AddressDTO.UNION_ID);
            wxhcSubmitOrder.setTotalPostage(0D);
            wxhcSubmitOrder.setSubBizType(EnumSubBizType.HAOYIKU.getCode());
            wxhcSubmitOrder.setOrderType(1);
            wxhcSubmitOrder.setSubOrderType(1);
            wxhcSubmitOrder.setOpenGId("");
            wxhcSubmitOrder.setCrossType(1);
            wxhcSubmitOrder.setFrom(AddressDTO.HYK_FROM);
            wxhcSubmitOrder.setReqPlatform(AddressDTO.HYK_ReqPlatform);
            wxhcSubmitOrder.setSubReqplatform(AddressDTO.HYK_SubReqPlatform);
            //结算

            com.webuy.buy.common.result.Result buyResult = payOrderApi.settlement(wxhcSubmitOrder);
            JSONArray js = JSONArray.fromObject(wxhcSubmitOrder);
            String array = js.toString();
            Object result2 = buyResult.getEntry();
            JSONObject jsonObject = new JSONObject();
            jsonObject = jsonObject.fromObject(result2);
            Long totalPayPrice = Long.valueOf(jsonObject.getString("totalPayPrice"));
            String pageToken = jsonObject.getString("pageToken");


            //支付 生成订单号
            wxhcSubmitOrder.setWxPayType(1);
            wxhcSubmitOrder.setAccountPayPrice(totalPayPrice);
            //为了防止重复调用
            wxhcSubmitOrder.setToken(pageToken);
            wxhcSubmitOrder.setBuyerMobile("15658797276");
            com.webuy.buy.common.result.Result<PayResultDTO> buy =payOrderApi.buy(wxhcSubmitOrder);
            PayResultDTO payResultDTO=buy.getEntry();
            Assert.assertNotNull(buy.getEntry().getBizOrderId());
            String price = totalPayPrice.toString();
            bizOrder.add(price);
            bizOrder.add(buy.getEntry().getBizOrderId());
        }


            return bizOrder;
    }
}
