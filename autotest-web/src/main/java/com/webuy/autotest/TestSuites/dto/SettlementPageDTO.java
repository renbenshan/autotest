package com.webuy.autotest.TestSuites.dto;

import com.aifocus.itemplatform.dto.IncreasePurchaseDTO;
import com.aifocus.wxhc.dto.DeliveryAddressDTO;
import com.aifocus.wxhc.dto.ExhibitionConfigDTO;
import com.webuy.buy.dto.AddressDTO;
import com.webuy.buy.dto.CouponForSettlementDTO;
import com.webuy.buy.dto.CustomInfoDTO;
import com.webuy.buy.dto.RedPackageDTO;
import com.webuy.buy.dto.insurance.SupplierInsuranceDTO;
import com.webuy.buy.dto.response.FreightInsuranceDTO;
import lombok.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author muze
 * @date 8/10/21 5:31 PM
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SettlementPageDTO {

    private Long TotalPayPrice;



}
