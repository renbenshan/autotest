package com.webuy.autotest.TestSuites;

import com.webuy.autotest.base.BaseTest;
import com.webuy.autotest.excel.util.CaseInfo;
import com.webuy.autotest.utils.ReadExcelDataExchangeCaseInfoUtil;
import com.webuy.autotest.utils.ReadExcelUtil;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author muze
 * @date 7/22/21 10:22 AM
 */
public class TestReadExcel extends BaseTest {



    @Test
    public void testReadExcel() throws IOException {

        ReadExcelDataExchangeCaseInfoUtil exchangeCaseInfoUtil =new ReadExcelDataExchangeCaseInfoUtil();
        String file = "/Users/admin/Autotest/autotest-web/src/main/resources/excel/testExcel.xlsx";
        String sheetName ="exhibition";
        List<CaseInfo> caseInfoList =exchangeCaseInfoUtil.readExcelData(file,sheetName);
        for (CaseInfo caseInfo:caseInfoList
             ) {
            System.out.println(caseInfo);
        }

    }

}
