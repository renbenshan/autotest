package com.webuy.autotest.TestSuites.enums;


/**
 * @author muze
 * @date 8/12/21 11:47 AM
 */
public enum EnumSubBizType {
    HAOYIKU(301L,"好衣库"),
    FENGXIANGJ(327L,"蜂享家"),
    ;

    private Long code;
    private String note;

    EnumSubBizType(Long code,String note){
        this.code=code;
        this.note=note;
    }

    public Long getCode() {
        return code;
    }

    public String getNote() {
        return note;
    }

    public boolean equals(Long code) {
        return this.code.equals(code);
    }

    public static EnumSubBizType getValue(Long code) {
        if (code == null) {
            return null;
        }
        for (EnumSubBizType values : EnumSubBizType.values()) {
            if (values.equals(code)) {
                return values;
            }
        }
        return null;
    }
}
