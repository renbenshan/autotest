package com.webuy.autotest.TestSuites.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author muze
 * @date 8/4/21 3:19 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExhibitionDTO implements Serializable {


    /**
     * 主键id
     */
    private Long exhibitionParkId;

    /**
     * 会场名
     */
    private String exhibitionParkName;

    /**
     * 业务子类型
     */
    private Long subBizType;


    /**
     * 会场类型，线上／线下
     */
    private Integer exhibitionParkType;


    /**
     * 关联中台会场的主键id
     */
    private Long exhibitionFromMiddleGroundId;


    /**
     * 会场开始时间
     */
    private Date gmtStart;

    /**
     * 会场结束时间
     */
    private Date gmtEnd;

    /**
     * 会场开售时间
     */
    private Date gmtOnline;

    /**
     * 更新时间
     */
    private Date gmtModify;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 逻辑删除
     */
    private Integer isDelete;

    /**
     * 是否是永久会场
     */
    private Boolean forever;

    /**
     * 店铺id，数据权限区分字段
     */
    private Long shopId;


    /**
     * 类目id
     */
    private Long categoryId;


    /**
     * 会场创建人ID
     */
    private Long creatorId;

    /**
     * 创建人类型
     */
    private Integer creatorType;

    /**
     * 会场状态
     *
     * @see
     */
    private Integer exhibitionParkStatus;

    /**
     * 会场审核状态
     */
    private Integer applyStatus;

    /**
     * 会场状态, 区别于 exhibitionParkStatus，该值只会在列表查的时候才有  queryExhibitionParkList
     */
    private Integer status;

    /**
     * 发布时间（点击按钮）
     */
    private Date releaseTime;

    /**
     * 会场排序
     */
    private Integer exhibitionParkSort;

    /**
     * 页面配置JSON
     */
    private String pageConfigJson;


    /**
     * 会场标签
     */
    private Integer exhibitionFlag;

    /**
     * 会场标签描述
     */
    private String exhibitionFlagDesc;

    /**
     * 供应商ID
     */
    private Long ownerId;


    /**
     * 会场时间状态
     */
    private Integer timeStatus;

    /**
     * 测试场景标记
     */
    private Integer testScene;

    /**
     * 会场限购数
     */
    private Integer exhibitionParkQuota;


}
