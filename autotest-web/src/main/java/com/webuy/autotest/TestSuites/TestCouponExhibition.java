package com.webuy.autotest.TestSuites;

import com.aifocus.base.common.result.Result;
import com.aifocus.itemplatform.api.WxhcPitemUploadApi;
import com.aifocus.itemplatform.common.enums.EnumChannel;
import com.aifocus.itemplatform.common.enums.EnumPitemStatus;
import com.aifocus.itemplatform.dto.WxhcSpuDTO;
import com.aifocus.itemplatform.dto.pitem.CreatePitemParams;
import com.alibaba.fastjson.JSON;
import com.webuy.autotest.TestSuites.enums.EnumSubBizType;
import com.webuy.autotest.apiImpl.CouponApi;
import com.webuy.autotest.apiImpl.ExhibiionInActivityApi;
import com.webuy.autotest.apiImpl.ExhibitionApi;
import com.webuy.autotest.apiImpl.ExhibitionPitemApi;
import com.webuy.autotest.base.BaseTest;
import com.webuy.exhibitionplatform.dto.CreateSaleActivityDTO;
import com.webuy.exhibitionplatform.enums.EnumExhibitionParkType;
import com.webuy.exhibitionplatform.request.ReleaseExhibitionParkRequest;
import com.webuy.exhibitionplatform.restructure.api.ExhibitionParkInnerFacadeApi;
import lombok.SneakyThrows;
import net.sf.json.JSONObject;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestCouponExhibition extends BaseTest {




    /** 先创建会场和会场活动*/
    @Autowired
    private ExhibitionApi exhibitionApi;


    @Autowired
    private ExhibiionInActivityApi  exhibitionInActivityApi;

    /** 创建优惠券关联会场 */
    @Autowired
    private CouponApi couponApi;

    /** 拉品 */
    @Autowired
    private ExhibitionPitemApi exhibitionPitemApi;
    /** 下单支付 */

    /**   创建带有优惠券的会场，并拉品
     *
     *int tags, int exhibitionParkType
     * */
    @Autowired
    private WxhcPitemUploadApi wxhcPitemUploadApi;

    @Autowired
    private ExhibitionParkInnerFacadeApi exhibitionParkInnerFacadeApi;



    @Test(testName = "创建带有优惠券的会场")
    public void creatCouponExhibition() throws ParseException {
        long subBizType = EnumSubBizType.HAOYIKU.getCode();
        int exhibitionParkType =EnumExhibitionParkType.ONLINE.getCode();;
        // 1.创建会场和活动
        HashMap exhibitionMap= this.creatExhinbitionAndActivity();
        Object exhibitionId= exhibitionMap.get("exhibitionId");
        Object activityId= exhibitionMap.get("activityId");

        // 将object类型转换成Long类型 来调用创建优惠券的方法
        Long SuccessExhibitionId = Long.parseLong(exhibitionId.toString());
        Long SuccessActivityId = Long.parseLong(activityId.toString());
        // 2.活动内拉品，并发布会场
        Result<List<Long>>  Pitemresult=this.AddspuAndSub(subBizType,exhibitionParkType,SuccessExhibitionId,SuccessActivityId);
        logger.info("[creatCouponExhibition] Pitemresult:"+Pitemresult);
        Result CouponResult=couponApi.creatCouponWithexhition(SuccessExhibitionId);
        logger.info("[creatCouponExhibition] CouponResult:"+CouponResult);


    }

    /** 1.创建会场和活动*/
    public  HashMap creatExhinbitionAndActivity()throws ParseException{

        com.aifocus.base.common.result.Result activityResult=exhibitionInActivityApi.addActivity();
        Object addActivityJson =activityResult.getEntry();
        JSONObject data = JSONObject.fromObject(addActivityJson);
        Object newEntry =data.getJSONObject("entry");
        JSONObject jsonObject = JSONObject.fromObject(newEntry);
        Long activityId= Long.valueOf(jsonObject.get("saleActivityId").toString());
        Long exhibitionId= Long.valueOf(jsonObject.get("exhibitionParkId").toString());
        logger.info(String.format("会场ID：%d,活动ID:%d", exhibitionId ,activityId));
        HashMap  exhibition = new HashMap();
        exhibition.put("exhibitionId",exhibitionId);
        exhibition.put("activityId",activityId);

        return exhibition;
    }

    /** 2.活动内拉品，并发布会场
     * return 会场的商品*/
    public com.aifocus.base.common.result.Result<List<Long>> AddspuAndSub(Long subBizType, Integer exhibitionParkType,Long exhibitionId,Long activityId)throws ParseException{
        CreatePitemParams createPItemParams =new CreatePitemParams();
        // 随便从会场找了一个商品，然后进行组装
        com.aifocus.itemplatform.common.result.Result<List<WxhcSpuDTO>> result =exhibitionPitemApi.queryPitemList();
        List<WxhcSpuDTO> list =result.getEntry();

        for (WxhcSpuDTO wxhcSpuDTO:list
        ) {
            Long spuId =wxhcSpuDTO.getId();

            List<Long> spuIdList =new ArrayList<>();
            spuIdList.add(spuId);
            createPItemParams.setSpuIdList(spuIdList);
            createPItemParams.setSubBizType(subBizType);
            createPItemParams.setTargetId(Long.valueOf(activityId));

            /**
             * 初始化商品状态
             * @see EnumPitemStatus
             * ONLINE:上架
             */
            createPItemParams.setPitemStatus(EnumPitemStatus.ONLINE);
            /**
             * {@link EnumChannel}
             * EnumChannel.HYK : 好衣库业务
             */

            if(subBizType ==301L){
                createPItemParams.setEnumChannel(EnumChannel.HYK);
            }else {
                createPItemParams.setEnumChannel(EnumChannel.BEE_HOME);
            }
            com.aifocus.base.common.result.Result<List<Long>> Pitemresult =wxhcPitemUploadApi.createExhibitionPitem(subBizType,exhibitionParkType,createPItemParams);


            /**
             * 将会场上架
             */
            ReleaseExhibitionParkRequest releaseExhibitionParkRequest =new ReleaseExhibitionParkRequest();
            releaseExhibitionParkRequest.setExhibitionParkId(exhibitionId);
            com.aifocus.base.common.result.Result<Void> voidResult =exhibitionParkInnerFacadeApi.releaseExhibitionPark(releaseExhibitionParkRequest);

            Result couponResult=couponApi.creatCouponWithexhition(Long.valueOf(exhibitionId));
            return Pitemresult;

        }

        return null;

    }




}

