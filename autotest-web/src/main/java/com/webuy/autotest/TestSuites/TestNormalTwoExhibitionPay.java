package com.webuy.autotest.TestSuites;

import com.aifocus.base.common.result.Result;
import com.aifocus.itemplatform.api.PitemApiService;
import com.aifocus.itemplatform.api.client.ItemClient;
import com.aifocus.itemplatform.dto.WxhcItemDTO;
import com.webuy.autotest.TestSuites.dto.AddressDTO;
import com.webuy.autotest.TestSuites.enums.EnumSubBizType;
import com.webuy.autotest.base.BaseTest;
import com.webuy.buy.api.PayOrderApi;
import com.webuy.buy.api.PayServiceApi;
import com.webuy.buy.api.order.SubBizOrderApi;
import com.webuy.buy.dto.ItemInfoDTO;
import com.webuy.buy.dto.PayResultDTO;
import com.webuy.buy.dto.WxhcSubmitOrderDTO;
import com.webuy.exhibitionplatform.restructure.api.ExhibitionParkFacadeApi;
import com.webuy.exhibitionplatform.restructure.dto.ExhibitionParkApiDTO;
import com.webuy.exhibitionplatform.restructure.query.ExhibitionParkFacadeQueryDTO;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class TestNormalTwoExhibitionPay extends BaseTest {
    @Autowired
    private ExhibitionParkFacadeApi exhibitionParkFacadeApi;

    @Autowired
    private ItemClient itemClient;
    @Autowired
    private PitemApiService pitemApiService;
    @Autowired
    private PayOrderApi payOrderApi;
    @Autowired
    private PayServiceApi payServiceApi;
    @Autowired
    private SubBizOrderApi subBizOrderApi;

    @Test(testName = "获取2个普通会场添加2个商品下单")
    public void testTwoExhibitionPay() throws ParseException {
        //获取2个会场
        List<ExhibitionParkApiDTO> exhibitionList = this.testQueryExhibitionId();
        // 遍历会场，获取会场第一个pItem下的第一个Item
        List<ItemInfoDTO> itemInfoList = new ArrayList<>();
        for (ExhibitionParkApiDTO exhibitionParkApiDTO : exhibitionList) {
            // 遍历会场获取exhibitionParkId
            Long exhibitionParkId = exhibitionParkApiDTO.getExhibitionParkId();
            // 根据exhibitionParkId查询pItem列表
            Result<List<Long>> result = pitemApiService.queryPitemIdsByExhibitionParkId(exhibitionParkId);
            List<Long> list = result.getEntry();
            // 取pItem列表第一个，查询Item列表
            List<WxhcItemDTO> WxhcItemDTOList = itemClient.getItemByPItemId(list.get(0));
            // 从Item列表获取第一个Item
            WxhcItemDTO wxhcItemDTO = WxhcItemDTOList.get(0);

            Long itemId = wxhcItemDTO.getId();
            logger.info("会场ID:" + exhibitionParkId + "会场下的itemId:" + itemId + "结算价格:" + wxhcItemDTO.getSettlementPrice() + "" + "B端价格:" + wxhcItemDTO.getPrice());

            //B端价格
            wxhcItemDTO.getSupplierPrice();
            // 活动b端价格
            //供货价
            wxhcItemDTO.getPrice();
            //结算价
            wxhcItemDTO.getSettlementPrice();
            wxhcItemDTO.getSupplierPrice();

            ItemInfoDTO itemInfoDTO = new ItemInfoDTO();
            itemInfoDTO.setItemId(itemId);
            itemInfoDTO.setItemNum(1);
            itemInfoDTO.setExhibitionId(exhibitionParkId);
            itemInfoDTO.setPitemId(wxhcItemDTO.getWxhcPitemId());
            itemInfoList.add(itemInfoDTO);
        }

        WxhcSubmitOrderDTO wxhcSubmitOrder = new WxhcSubmitOrderDTO();
        wxhcSubmitOrder.setItemList(itemInfoList);
        wxhcSubmitOrder.setCuserId(AddressDTO.HYK_CUSER_ID);
        wxhcSubmitOrder.setBuyerId(AddressDTO.BUYER_ID);
        wxhcSubmitOrder.setMobile(AddressDTO.MOBILE);
        wxhcSubmitOrder.setAddress(AddressDTO.ADRESS);
        wxhcSubmitOrder.setPartAddress(AddressDTO.PART_ADRESS);
        wxhcSubmitOrder.setAppId(AddressDTO.APPID);
        wxhcSubmitOrder.setAreaCode(AddressDTO.AREA_CODE);
        wxhcSubmitOrder.setCityCode(AddressDTO.CITY_CODE);
        wxhcSubmitOrder.setProvinceCode(AddressDTO.PROVINCE_CODE);
        wxhcSubmitOrder.setDeliverAddressId(AddressDTO.DELIVER_ADDRESS_ID);
        wxhcSubmitOrder.setReceiverName(AddressDTO.RECEIVER_NAME);
        wxhcSubmitOrder.setReceiverMobile(AddressDTO.RECEIVER_MOBILE);
        wxhcSubmitOrder.setOpenId(AddressDTO.OPEN_ID);
        wxhcSubmitOrder.setUnionId(AddressDTO.UNION_ID);
        wxhcSubmitOrder.setTotalPostage(0D);
        wxhcSubmitOrder.setSubBizType(EnumSubBizType.HAOYIKU.getCode());
        wxhcSubmitOrder.setOrderType(1);
        wxhcSubmitOrder.setSubOrderType(1);
        wxhcSubmitOrder.setOpenGId("");
        wxhcSubmitOrder.setCrossType(1);
        wxhcSubmitOrder.setFrom(AddressDTO.HYK_FROM);
        wxhcSubmitOrder.setReqPlatform(AddressDTO.HYK_ReqPlatform);
        wxhcSubmitOrder.setSubReqplatform(AddressDTO.HYK_SubReqPlatform);


        // 结算
        com.webuy.buy.common.result.Result buyResult = payOrderApi.settlement(wxhcSubmitOrder);

        JSONObject jsonObject = JSONObject.fromObject(buyResult.getEntry());
        Long totalPayPrice = Long.valueOf(jsonObject.getString("totalPayPrice"));
        String pageToken = jsonObject.getString("pageToken");

        /**
         * 支付
         */
        wxhcSubmitOrder.setWxPayType(1);
        wxhcSubmitOrder.setAccountPayPrice(totalPayPrice);
        //为了防止重复调用
        wxhcSubmitOrder.setToken(pageToken);
        wxhcSubmitOrder.setBuyerMobile("18756939317");
        wxhcSubmitOrder.setRealName("百事");
        wxhcSubmitOrder.setHykSettlementFrom(1);
        com.webuy.buy.common.result.Result<PayResultDTO> buy = payOrderApi.buy(wxhcSubmitOrder);
        List<String> bizOrderIdList = buy.getEntry().getBizOrderIdList().stream().distinct().collect(Collectors.toList());
        logger.info("主订单id" + bizOrderIdList);

    }

    public List<ExhibitionParkApiDTO> testQueryExhibitionId() throws ParseException {
        ExhibitionParkFacadeQueryDTO exhibitionParkFacadeQueryDTO = new ExhibitionParkFacadeQueryDTO();
        exhibitionParkFacadeQueryDTO.setSubBizType(301L);
        //线上类型
        exhibitionParkFacadeQueryDTO.setExhibitionParkType(1);
        //timeStatus   时间状态
        List<Integer> list = new ArrayList<>();
        list.add(2);
        exhibitionParkFacadeQueryDTO.setTimeStatusList(list);

        //actionStatus 活动状态
        List<Integer> exhibitionParkStatusList = new ArrayList<>();
        exhibitionParkStatusList.add(102);
        exhibitionParkFacadeQueryDTO.setExhibitionParkStatusList(exhibitionParkStatusList);

        Result<List<ExhibitionParkApiDTO>> entry = exhibitionParkFacadeApi
                .queryExhibitionParkList(exhibitionParkFacadeQueryDTO);
        List<ExhibitionParkApiDTO> exhibitionParkApiDTOList = entry.getEntry();
        List<ExhibitionParkApiDTO> newExhibitonList = new ArrayList<>();
        for (ExhibitionParkApiDTO exhibitionParkApiDTO : exhibitionParkApiDTOList
        ) {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
            String date = simpleDateFormat.format(new Date());
            Date time = simpleDateFormat.parse(date);

            //getStatus:4 线上进行中  tag:100 普通会场
            if (exhibitionParkApiDTO.getStatus() == 4 && exhibitionParkApiDTO.getGmtEnd().after(time)) {
                newExhibitonList.add(exhibitionParkApiDTO);
                logger.info("会场ID：" + exhibitionParkApiDTO.getExhibitionParkId() + ",会场的起止时间" + exhibitionParkApiDTO.getGmtStart().getTime() + "~" + exhibitionParkApiDTO.getGmtEnd().getTime());
                if (newExhibitonList.size() <= 0) {
                    logger.info("满足类型的会场数据为空");
                }
                if (newExhibitonList.size() == 2) {
                    break;
                }

            }
        }

        return newExhibitonList;
    }

}