package com.webuy.autotest.test;

import com.webuy.autotest.base.BaseTest;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author muze
 * @date 8/18/21 3:37 PM
 */
public class TestDate extends BaseTest {

    @Test
    public void testdate() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
//        Date GmtStart= simpleDateFormat.parse("2021-08-13 00:00:00");
//        System.out.println(GmtStart);
//        Date GmtEnd= simpleDateFormat.parse("2021-08-21 00:00:00");
//        System.out.println(GmtEnd);

//        String currentTime =simpleDateFormat.format(new Date()); //当前时间
        Date GmtStart =simpleDateFormat.parse(simpleDateFormat.format(new Date()));
        System.out.println(GmtStart);
        String endTime=simpleDateFormat.format(new Date(GmtStart.getTime() +24*60*60*5));
        Date  GmtEnd=simpleDateFormat.parse(endTime);
        System.out.println(GmtEnd);

    }
}
