package com.webuy.autotest.test;

import com.webuy.autotest.apiImpl.PromotionApi;
import com.webuy.autotest.base.BaseTest;
import com.webuy.autotest.dto.CreatePromotionDTO;
import com.webuy.exhibitionplatform.api.ExhibitionParkApi;
import com.webuy.exhibitionplatform.dto.ExhibitionParkApiDTO;
import com.webuy.promotion.api.PromotionBackendNewApi;
import com.webuy.promotion.api.vo.Promotion;
import com.webuy.promotion.api.vo.Result;
import freemarker.template.SimpleDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author muze
 * @date 8/19/21 5:31 PM
 */
public class TestPromotionApi extends BaseTest {
    @Autowired
    private PromotionApi promotionApi;
    @Autowired
    private ExhibitionParkApi exhibitionParkApi;


    @Test
    public void testCreatePromotion() throws ParseException {
        CreatePromotionDTO createPromotionDTO =new CreatePromotionDTO();
        createPromotionDTO.setTitle("沐泽自动化创建的促销一口价");
        createPromotionDTO.setBusinessCode("1000");
        createPromotionDTO.setPromotionType("1002");
        createPromotionDTO.setActionValue("5");
        createPromotionDTO.setBackActionValue("5");
        createPromotionDTO.setConditionValue("3");
        createPromotionDTO.setExhibitionId(100315457L);
        createPromotionDTO.setUnderTake(1);

        Result<Promotion> result =promotionApi.createPromotion(createPromotionDTO);
        Promotion promotionResult =result.getEntry();
        System.out.println(promotionResult);

    }
}
