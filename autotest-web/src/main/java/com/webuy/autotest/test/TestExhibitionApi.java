package com.webuy.autotest.test;

import com.aifocus.base.common.result.Result;
import com.webuy.autotest.apiImpl.ExhibitionApi;
import com.webuy.autotest.base.BaseTest;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

/**
 * @author muze
 * @date 8/20/21 2:54 PM
 */
public class TestExhibitionApi extends BaseTest {
    @Autowired
    private ExhibitionApi exhibitionApi;
    @SneakyThrows
    @Test
    public void testCreateExhibition(){
        Integer tags =null;
        Integer exhibitionParkType=1;
        Result result =exhibitionApi.creatExhibition(tags,exhibitionParkType);
        Object entry =result.getEntry();
        logger.info("Object:"+result);
    }

    @SneakyThrows
    @Test
    public void testCreateShExhibition(){
        Integer tags = null;
        Integer exhibitionParkType = 1;
        Result result = exhibitionApi.createShExhibition(exhibitionParkType);
        Object entry = result.getEntry();
        logger.info("Object:" + result);
    }
}
