package com.webuy.autotest.test;

import com.aifocus.itemplatform.common.result.Result;
import com.aifocus.itemplatform.dto.pitem.CreatePitemParams;
import com.webuy.autotest.apiImpl.ExhibitionPitemApi;
import com.webuy.autotest.base.BaseTest;
import com.webuy.exhibitionplatform.enums.EnumExhibitionParkType;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.util.List;

/**
 * @author muze
 * @date 8/17/21 3:55 PM
 */
public class TestPitemApi extends BaseTest {
    @Autowired
    private ExhibitionPitemApi exhibitionPitemApi;

    @Test
    public void testQuerryPitem(){

        Result result =exhibitionPitemApi.queryPitemList();
        System.out.println("result===="+result);

    }

    @Test
    public void addSpu() throws ParseException {
        Long subBizType =301L;
        Integer exhibitionParkType = EnumExhibitionParkType.ONLINE.getCode();
       com.aifocus.base.common.result.Result<List<Long>> result = exhibitionPitemApi.addSpu(subBizType,exhibitionParkType);
        List<Long> PitemIdList =result.getEntry();
        for (Long pitem:PitemIdList
             ) {
            System.out.println("pitem=="+pitem);
        }
    }
}
