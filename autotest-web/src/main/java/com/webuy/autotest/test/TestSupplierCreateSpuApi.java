package com.webuy.autotest.test;

import com.aifocus.base.common.result.Result;
import com.webuy.autotest.apiImpl.SupplierCreateSpuApi;
import com.webuy.autotest.base.BaseTest;
import com.webuy.autotest.dto.CretaeSpuDTO;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.Random;

/**
 * @author muze
 * @date 8/27/21 10:37 AM
 */
public class TestSupplierCreateSpuApi extends BaseTest {
    @Autowired
    private SupplierCreateSpuApi supplierCreateSpuApi;


    @Test
    public void testSupplierCreateSpu(){
        CretaeSpuDTO cretaeSpuDTO =new CretaeSpuDTO();

        cretaeSpuDTO.setSubBizType(301L);
        cretaeSpuDTO.setSupplierId(11755L);
        cretaeSpuDTO.setBrandId(54180L);
        cretaeSpuDTO.setWxhcCategoryId(10047L);
        cretaeSpuDTO.setInventory(9999999);
        cretaeSpuDTO.setOriginalPrice(1000L);
        cretaeSpuDTO.setSettlementPrice(1000L);
        cretaeSpuDTO.setSupplierPrice(900L);
        cretaeSpuDTO.setTaobaoPrice(1000L);

        Result result=supplierCreateSpuApi.createSpu(cretaeSpuDTO);
        logger.info("entry msg:{}"+ result);
    }

}
