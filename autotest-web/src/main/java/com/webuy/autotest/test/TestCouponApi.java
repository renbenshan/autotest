package com.webuy.autotest.test;

import com.aifocus.base.common.result.Result;
import com.webuy.autotest.apiImpl.CouponApi;
import com.webuy.autotest.base.BaseTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

/**
 * @author muze
 * @date 8/18/21 4:35 PM
 */
public class TestCouponApi extends BaseTest {
    @Autowired
    private CouponApi couponApi;
    @Test
    public void creatCouponTemplate(){
        Long exhibitionId = 100316827L;
        Result result=couponApi.creatCouponWithexhition(exhibitionId);
        System.out.println(result.getEntry());
    }
}
