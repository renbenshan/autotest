package com.webuy.autotest.test;

import com.aifocus.base.common.result.Result;
import com.webuy.autotest.apiImpl.ExhibiionInActivityApi;
import com.webuy.autotest.base.BaseTest;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

/**
 * @author muze
 * @date 8/17/21 12:02 PM
 */
public class TestCreatActivityApi extends BaseTest {


    @Autowired
    private ExhibiionInActivityApi exhibiionInActivityApi;
    @SneakyThrows
    @Test
    public void testCreatActivity(){
        Result result =exhibiionInActivityApi.addActivity();
        logger.info("结果数据："+result);
    }

    @SneakyThrows
    @Test
    public void getSaleActivityList(){
        Long exhibitionParkId = 100317723L;
        Result result =exhibiionInActivityApi.getSaleActivityList(exhibitionParkId);
        logger.info("结果数据："+result);
    }
}
