package com.webuy.autotest.demo;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

/**
 * @author muze
 * @date 7/19/21 2:29 AM
 */
public class TestDemo {

    @Test
    public void test1 (){
        Assert.assertEquals(1,2);
    }


    @Test
    public void test2(){
        Assert.assertEquals(1,1);
    }

    @Test
    public void test3(){
        Reporter.log("日志擦讯");
        throw new RuntimeException("出现erro");
    }
}
