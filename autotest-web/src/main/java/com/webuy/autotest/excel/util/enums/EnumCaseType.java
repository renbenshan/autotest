package com.webuy.autotest.excel.util.enums;

/**
 * @author muze
 * @date 7/23/21 3:28 PM
 */
public enum EnumCaseType {

    DUBBO(1,"dubbo接口"),
    HTTP(2,"http接口"),
    ;

    EnumCaseType(Integer code, String note) {
        this.code = code;
        this.note = note;
    }

    private Integer code;
    private String note;

    public Integer getCode() {
        return code;
    }

    public String getNote() {
        return note;
    }

    public boolean equals(Integer code) {
        return this.code.equals(code);
    }

    public static EnumCaseType getValue(Integer code) {
        if (code == null) {
            return null;
        }
        for (EnumCaseType values : EnumCaseType.values()) {
            if (values.equals(code)) {
                return values;
            }
        }
        return null;
    }
}
