package com.webuy.autotest.excel.util.enums;

/**
 * @author muze
 * @date 7/23/21 3:55 PM
 */
public enum EnumLevelType {

    P0(1,"主流程case"),
    P1(2,"功能case"),
    P2(3,"低级case")
    ;

    EnumLevelType(Integer code,String note){
        this.code=code;
        this.note=note;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    private Integer code;
    private String note;


    public boolean equals(Integer code) {
        return this.code.equals(code);
    }


    public static EnumLevelType getValue(Integer code) {
        if (code == null) {
            return null;
        }
        for (EnumLevelType values : EnumLevelType.values()) {
            if (values.equals(code)) {
                return values;
            }
        }
        return null;
    }


}
