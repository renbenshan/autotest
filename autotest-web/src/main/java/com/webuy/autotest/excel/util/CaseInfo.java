package com.webuy.autotest.excel.util;

import com.webuy.autotest.excel.util.enums.EnumLevelType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author muze
 * @date 7/23/21 3:22 PM
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CaseInfo {

    /**
     * 接口名称
     */
    private String caseName;

    /***
     * 用例参数
     */
    private Map<String,String> caseParam;

    /**
     * 预期结果
     */
    private Map<String,String> caseExpect;

//    /**
//     * 接口返回码
//     */
//    private String code;
    /**
     * 状态
     */

    private Boolean status;

    /**
     * 用例等级
     * {@link EnumLevelType}
     */

    private String level;


//    /**
//     * 接口的类型
//     */
//    private String CaseType;
//
//    /**
//     * api的路径
//     */
//    private String apiPath;
//
//    /**
//     * api的方法
//     */
//    private String apiMethod;

}
