package com.webuy.autotest.base;

import com.alibaba.fastjson.JSON;
import com.webuy.autotest.AutoTestWebApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import java.io.Serializable;

/**
 * @author muze
 * @date 7/20/21 3:29 PM
 */
@SpringBootTest(classes = {AutoTestWebApplication.class})
public class BaseTest extends AbstractTestNGSpringContextTests {

    protected static void print(Serializable serializable) {
        System.err.print("\n\n\n\n\n\n" + JSON.toJSONString(serializable) + "\n\n\n\n\n\n\n\n");
    }
}
