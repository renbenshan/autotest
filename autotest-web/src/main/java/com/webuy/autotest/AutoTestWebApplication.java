package com.webuy.autotest;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

/**
 * @author muze
 * @date 7/19/21 12:25 AM
 */
@SpringBootApplication
@ComponentScan(value = {"com.aifocus.base.mq","com.webuy.autotest","com.aifocus.itemplatform.api"})
@DubboComponentScan(basePackages = "com.webuy.autotest.apiImpl")
// ,"classpath:spring/stock-client.xml"
@ImportResource(locations = {"classpath:spring/itemplatform-client.xml"})
@PropertySource(value = {"classpath:application.properties"})
public class AutoTestWebApplication {


    public static void main(String[] args) {
        SpringApplication.run(AutoTestWebApplication.class,args);
    }

}

