package com.webuy.autotest.utils;

import com.webuy.autotest.excel.util.CaseInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author muze
 * @date 8/16/21 11:53 AM
 */
public class ReadExcelDataExchangeCaseInfoUtil {


    public List<CaseInfo> readExcelData(String file,String sheetName) throws IOException {
        ReadExcelUtil readExcelUtil =new ReadExcelUtil();
        List<Map<String, String>> list =readExcelUtil.readExcelData(file,sheetName);

        Map<String,String> caseParamMap =new HashMap<>();
        Map<String,String> caseExpectMap =new HashMap<>();

        List<CaseInfo> caseInfoList =new ArrayList<>();

        for (Map<String, String> object: list) {
            CaseInfo caseInfo =new CaseInfo();

            if (object.get("caseName") != null && object.get("caseName") != "") {
                caseInfo.setCaseName(object.get("caseName"));
            } else {
                throw new RuntimeException("用例名称不能为空");
            }

            if (object.get("caseParam") != null && object.get("caseParam") != "") {
                caseParamMap.put("caseParam", object.get("caseParam"));
                caseInfo.setCaseParam(caseParamMap);
            } else {
                throw new RuntimeException("用例入参不能为空");
            }

            if (object.get("caseExpect") != null && object.get("caseExpect") != "") {
                caseExpectMap.put("caseExpect", object.get("caseExpect"));
                caseInfo.setCaseExpect(caseExpectMap);
            } else {
                throw new RuntimeException("用例预期结果不能为空");
            }

            caseInfo.setStatus(Boolean.valueOf(object.get("sttus")));

            caseInfo.setLevel(object.get("level"));
            caseInfoList.add(caseInfo);

        }
        return caseInfoList;
    }
}
