package com.webuy.autotest.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;

/**
 * @author muze
 * @date 7/22/21 4:07 PM
 */
public class ExcleReadNullRow {

    public static String getValue(XSSFCell xssfCell) {
        if (xssfCell ==null){return ""; }
        if (xssfCell.getCellType() == xssfCell.CELL_TYPE_BOOLEAN) {
            return String.valueOf(xssfCell.getBooleanCellValue());
        } else if (xssfCell.getCellType() == xssfCell.CELL_TYPE_NUMERIC) {
            return String.valueOf(xssfCell.getNumericCellValue());
        } else {
            return String.valueOf(xssfCell.getStringCellValue());
        }
    }
//    public String getType(XSSFCell cell) {
//        String value = null;
//        //简单的查检列类型
////        System.out.println("cell.getCellType():"+cell.getCellType());
//        switch(cell.getCellType())  {
//
//            case XSSFCell.CELL_TYPE_STRING://字符串
////            	System.out.println("HSSFCell.CELL_TYPE_STRING:"+HSSFCell.CELL_TYPE_STRING);
//                value = cell.getRichStringCellValue().toString();
//                System.out.println(value);
//                break;
//            case XSSFCell.CELL_TYPE_NUMERIC://数字
////            	System.out.println("HSSFCell.CELL_TYPE_NUMERIC:"+HSSFCell.CELL_TYPE_NUMERIC);
//                long dd = (long)cell.getNumericCellValue();
//                value = dd+"";
//                System.out.println(value);
//                break;
//            case XSSFCell.CELL_TYPE_BLANK:
////            	System.out.println("HSSFCell.CELL_TYPE_BLANK:"+HSSFCell.CELL_TYPE_BLANK);
//                value = "";
//                System.out.println(value);
//                break;
//            case XSSFCell.CELL_TYPE_FORMULA:
////            	System.out.println("HSSFCell.CELL_TYPE_FORMULA:"+HSSFCell.CELL_TYPE_FORMULA);
//                value = String.valueOf(cell.getCellFormula());
//                System.out.println(value);
//                break;
//            case XSSFCell.CELL_TYPE_BOOLEAN://boolean型值
////            	System.out.println("HSSFCell.CELL_TYPE_BOOLEAN:"+HSSFCell.CELL_TYPE_BOOLEAN);
//                value = String.valueOf(cell.getBooleanCellValue());
//                System.out.println(value);
//                break;
//            case XSSFCell.CELL_TYPE_ERROR:
////            	System.out.println("HSSFCell.CELL_TYPE_ERROR:"+HSSFCell.CELL_TYPE_ERROR);
//                value = String.valueOf(cell.getErrorCellValue());
//                System.out.println(value);
//                break;
//            default:
////            	System.out.println("default");
//                break;
//        }
//        return value;
//    }

}