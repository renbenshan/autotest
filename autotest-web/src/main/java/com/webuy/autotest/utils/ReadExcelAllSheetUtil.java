package com.webuy.autotest.utils;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static com.webuy.autotest.utils.ExcleReadNullRow.getValue;
import static org.apache.poi.ss.usermodel.CellType.STRING;

/**
 * @author muze
 * @date 7/22/21 5:43 PM
 */
public class ReadExcelAllSheetUtil {


    public List<Map<String,String>> readExcelAllSheetData(String file) throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(new File(file)));

        XSSFSheet sheet = null;

        //获取表头
        XSSFRow headRow =sheet.getRow(0);

        List<Map<String, String>> list = new ArrayList<Map<String, String>>();

        for (int s = 0; s < workbook.getNumberOfSheets(); s++) {

            sheet =workbook.getSheetAt(s);
            for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {

                XSSFRow row=sheet.getRow(i);  //每一行
                Map<String, String> map = new HashMap<String, String>();


                for (int j = 0; j < row.getPhysicalNumberOfCells(); j++) {

                    XSSFCell cell = row.getCell(j); //每个单元格
                    XSSFCell xssfCellTitleCell = headRow.getCell(j);

                    map.put(getValue(xssfCellTitleCell), getValue(cell));

                    if (cell != null) {
                        ExcleReadNullRow excleReadNullRow =new ExcleReadNullRow();
                        ExcleReadNullRow.getValue(cell);
                    } else {
                        continue;
                    }
                    cell.setCellType(STRING);

                    // 把每列的值都存放如定义好的数组中
                    String data= cell.getStringCellValue();

                }
            }
            System.out.println("===========" + sheet.getSheetName()+"===读表完成");
        }
        return list;
    }


}
