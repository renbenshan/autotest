package com.webuy.autotest.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.IOException;
import java.util.*;
import static com.webuy.autotest.utils.ExcleReadNullRow.getValue;


/**
 * @author muze
 * @date 7/21/21 4:28 PM
 */
@Slf4j
public class ReadExcelUtil {


    public  List<Map<String, String>>readExcelData(String file,String sheetName) throws IOException {

        XSSFWorkbook xssfWorkbook= new XSSFWorkbook(file);

        // 指定sheet 表
        XSSFSheet sheet = xssfWorkbook.getSheet(sheetName);

//        XSSFSheet sheet =xssfWorkbook.getSheetAt(0);
        // 获取表头
        XSSFRow headRow =sheet.getRow(0);

        List<Map<String, String>> list = new ArrayList<Map<String, String>>();

        //循环每一行
        for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
            XSSFRow xssfRow = sheet.getRow(rowNum);

            if (xssfRow == null) {
                continue;
            }
            Map<String, String> map = new HashMap<String, String>();
            // 循环每一列Cell
            for (int cellNum = 0; cellNum <headRow.getLastCellNum(); cellNum++) {

                XSSFCell xssfCell = xssfRow.getCell(cellNum);
                XSSFCell xssfCellTitleCell = headRow.getCell(cellNum);
                /**
                 * getValue(xssfCellTitleCell)  所有表头字段作为key
                 * getValue(xssfCell)  所有的表格内容作为value
                 */

                map.put(getValue(xssfCellTitleCell), getValue(xssfCell));

            }

            list.add(map);

        }
        return list;
    }


}
