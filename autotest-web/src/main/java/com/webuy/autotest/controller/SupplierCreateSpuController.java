package com.webuy.autotest.controller;

import com.aifocus.base.common.result.Result;
import com.webuy.autotest.apiImpl.SupplierCreateSpuApi;
import com.webuy.autotest.dto.CretaeSpuDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author muze
 * @date 8/27/21 4:29 PM
 */
@RestController
@RequestMapping("/spu")
public class SupplierCreateSpuController {
    @Autowired
    private SupplierCreateSpuApi supplierCreateSpuApi;


    @RequestMapping(value ="/hyk/createSpu", method = RequestMethod.POST)
    public Result createSpu(@RequestBody CretaeSpuDTO cretaeSpuDTO){

        Result result =supplierCreateSpuApi.createSpu(cretaeSpuDTO);
        return result;
    }
}
