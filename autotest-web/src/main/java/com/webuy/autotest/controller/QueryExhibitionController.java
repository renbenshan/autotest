package com.webuy.autotest.controller;

import com.aifocus.base.common.result.Result;
import com.aifocus.itemplatform.api.PitemApiService;
import com.aifocus.itemplatform.query.WxhcPitemQuery;
import com.webuy.autotest.apiImpl.TestExhibitionApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;


/**
 * @author muze
 * @date 7/20/21 11:31 AM
 */

@Controller
@RequestMapping("queryExhibition")
public class QueryExhibitionController {


    @Autowired
    private PitemApiService pitemApiService;

    @Resource
    private TestExhibitionApi testExhibitionApi;



    @ResponseBody
    @RequestMapping("/testQueryExhibiton")
    public Result testQueryExhibiton () {

        WxhcPitemQuery wxhcPitemQuery =new WxhcPitemQuery();
        wxhcPitemQuery.setExhibitionParkId(100315484L);
        return Result.buildSuccess(pitemApiService.queryDTO(wxhcPitemQuery));

    }

    @ResponseBody
    @RequestMapping("/testExhibitionApi")
    public Result testExhibitionApi () {

        return Result.buildSuccess(testExhibitionApi.queryDTO());

    }
}
