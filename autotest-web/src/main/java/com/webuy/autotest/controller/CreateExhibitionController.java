package com.webuy.autotest.controller;

import com.aifocus.base.common.result.Result;
import com.webuy.autotest.apiImpl.ExhibitionApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.text.ParseException;

/**
 * @author muze
 * @date 8/13/21 3:09 PM
 */

@Controller
@RequestMapping("/create")
public class CreateExhibitionController {

    @Autowired(required = true)
    private ExhibitionApi exhibitionApi;

    @ResponseBody
    @RequestMapping("/exhibition")
    public Result creatExhibition() throws ParseException {

        Integer tags =101;
        Integer ExhibitionParkType =1;
        Result result=exhibitionApi.creatExhibition(tags,ExhibitionParkType);

           return Result.buildSuccess(result);

    }
}
